from django.contrib import admin

# Register your models here.
from .models import Lesson, Question, MultChoice, TF

admin.site.register(Lesson)
admin.site.register(Question)
# admin.site.register(SearchTerms)
admin.site.register(MultChoice)
admin.site.register(TF)
