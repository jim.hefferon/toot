import datetime
from django.db import models
from django.utils import timezone

from django.contrib.auth.models import User # to point to

from django.db.models.signals import post_save  # tell User to update TootUser
from django.dispatch import receiver

import datetime # for DateTimeField; makemigrations does not like auto_now_add=True, so pass the executable datetime.datetime.now
from django.utils.timezone import now

from lessons.models_parameters import * # some global definitions used by multiple mods
# AMSMATH = 'amsmath'
# BEAMER = 'beamer'
# CONTEXT = 'context'
# LATEX = 'latex'
# TEX = 'tex'
# LESSON_TYPES = (
#     (AMSMATH, 'amsmath'),
#     (BEAMER, 'beamer'),
#     (CONTEXT, 'context'),
#     (LATEX, 'latex'),
#     (TEX, 'tex'),
#     )
# TEXT = 'TE'
# MULT_CHOICE = 'MC'
# TRUE_FALSE = 'TF'
# FILL_IN = 'FI'
# QUESTION_TYPES = (
#     (TEXT, 'Text'),
#     (MULT_CHOICE, 'Multiple choice'),
#     (TRUE_FALSE, 'True/False'),
#     (FILL_IN, 'Fill in the blank'),
#     )
# NUM_CHARS_IN_LESSON_TYPE = 25

class LessonType(models.Model):
    """Kinds of lessons available
    """
    # TODO: add html used to display?
    lesson_type = models.CharField(
        max_length=NUM_CHARS_IN_LESSON_TYPE,
        choices=LESSON_TYPES,
        default=LATEX,
        db_index=True,
        help_text = 'Types of lessons available')
    available = models.BooleanField(default=False,
                                    help_text='Is this lesson offered?')   

    def __str__(self):
        return self.lesson_type

# def _get_lesson_type_latex_pk():
#     """Return the primary key of the LessonType instance with lesson_type=LATEX
#     """
#     LessonType.objects.get(lesson_type=LATEX)

LESSON_DOC_PATH_ROOT="/home/ftpmaint/src/toot/toot/doc/"
class Lesson(models.Model):
    """Each page is a lesson
    """
    lesson_type = models.ForeignKey(LessonType, on_delete=models.CASCADE, null=True) 
    page_no = models.IntegerField(default=0,db_index=True)
    transcript_fn = models.FilePathField(path=LESSON_DOC_PATH_ROOT, 
                                         allow_folders=True,
                                         default='transcript.pdf',
                                         help_text="Path to lesson transcript, below the root")
    title = models.CharField(max_length=200) 
    intro_text = models.TextField(blank=True)  # not required 
    body_text = models.TextField(blank=True)

    class Meta:
        unique_together = (('lesson_type','page_no'),)

    def __str__(self):
        return self.title


class LessonDocument(models.Model):
    """Each lesson has some associated documents that the user can download.
    """
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE) 
    src_path = models.FilePathField(path=LESSON_DOC_PATH_ROOT, allow_folders=True,default='',
                                help_text="Path to source document, below the root")
    img_path = models.FilePathField(path=LESSON_DOC_PATH_ROOT, allow_folders=True,default='',
                                help_text="Path to image document (PDF), below the root")
    name = models.CharField(max_length=100,
                            help_text="File name shown to user during lesson")

    def __str__(self):
        return self.name


LESSON_GRAPHICS_PATH_ROOT = "/home/ftpmaint/src/toot/toot/toot/static/graphics"
LESSON_VID_PATH_ROOT = "/home/ftpmaint/src/toot/toot/toot/static/video"
class VideoSeries(models.Model):
    """Different videos showing different platforms, possibly languages, etc.
    """
    lesson_type = models.ForeignKey(LessonType, on_delete=models.CASCADE)
    series = models.CharField(max_length = 100, unique=True, db_index = True,
                              null = True, # for migrations
                              help_text="identifier of this series")
    description = models.CharField(max_length=200,
                                   help_text="Description shown to user choosing lesson")
    
    class Meta:
        unique_together = ('lesson_type', 'series')

    def __str__(self):
        return self.description


class LessonVideo(models.Model):
    """URL for the lesson's video, and image in the given series
    """
    series = models.ForeignKey(VideoSeries, on_delete=models.CASCADE) 
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE) 
    video_url = models.URLField(max_length=200,null=False,
                          help_text='URL to video for this lesson')
    image_fn = models.CharField(max_length=200,default=None,null=True,
                                help_text='file name of image for video for this lesson')

    def __str__(self):
        return self.url


MULT_CHOICE_MAX_LENGTH = 20
FILL_IN_MAX_LENGTH = 100
class Question(models.Model):
    """Each question has an associated lesson (page), and is one of a small
    number of question types.
    """
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE) # page it is on
    question_type = models.CharField(
        max_length=2,
        choices=QUESTION_TYPES,
        default=TEXT,
    )    
    question_text = models.CharField(max_length=400,
                                     help_text="Body of question") # Body of q
    # run_tex = models.BooleanField(default = False,
    #                               help_text="Run latex to test the response?") # Run LaTeX to test?
    # text_response = models.TextField(max_length = 2000, blank=True,
    #                                  help_text="User's response") 

    def __str__(self):
        return self.question_text



# class SearchTerms(models.Model):
#     """Terms to search for in answers to quiz questions
#     """
#     question = models.ForeignKey(Question, on_delete=models.CASCADE) # 
#     term = models.CharField(max_length=100) 
#     comment = models.CharField(max_length=1000,
#                                help_text="What to say if term not found") 

#     def __str__(self):
#         return str(self.term)



class TextResponse(models.Model):
    """Response to text question
    """
    question = models.ForeignKey(Question, on_delete=models.CASCADE, db_index=True) # 
    text = models.TextField(max_length = 2000, blank=True,
                                     help_text="User's response") 
    comment = models.CharField(max_length=1000,
                               help_text="What to say if term mistaken") 

    def __str__(self):
        return str(self.text)


class MultChoice(models.Model):
    """Multiple choice questions need key-value list of options, and an 
    optional comment if that choice is made. 
    """
    question = models.ForeignKey(Question, on_delete=models.CASCADE, db_index=True)
    number_choices = models.SmallIntegerField(default=2)  # how many options?
    answer = models.SmallIntegerField(default=0) # which is right: 1, 2 ..?
    key_one = models.CharField(max_length=MULT_CHOICE_MAX_LENGTH, default="1")
    text_one = models.CharField(max_length=200, default="")
    comment_one = models.CharField(max_length=1000,
                                   blank=True,
                                   default="")
    key_two = models.CharField(max_length=MULT_CHOICE_MAX_LENGTH, 
                               default="2")
    text_two = models.CharField(max_length=200, default="")
    comment_two = models.CharField(max_length=1000, 
                                   blank=True,
                                   default="")
    key_three = models.CharField(max_length=MULT_CHOICE_MAX_LENGTH, 
                                 default="3",
                                 blank=True)
    text_three = models.CharField(max_length=200,
                                  blank=True,  # if only two choices
                                  default="")
    comment_three = models.CharField(max_length=1000, 
                                     blank=True,
                                     default="")
    key_four = models.CharField(max_length=MULT_CHOICE_MAX_LENGTH, 
                                default="4", 
                                blank=True)
    text_four = models.CharField(max_length=200, 
                                  blank=True,  # if few choices
                                 default="")
    comment_four = models.CharField(max_length=1000, default="")
    key_five = models.CharField(max_length=MULT_CHOICE_MAX_LENGTH, 
                                default="5",
                                blank=True)
    text_five = models.CharField(max_length=200, 
                                 blank=True,  # if few choices
                                 default="")
    comment_five = models.CharField(max_length=1000, 
                                    blank=True,
                                    default="")

    def __str__(self):
        return str(self.answer)


class TF(models.Model):
    """Answer to a True/False question
    """
    question = models.ForeignKey(Question, on_delete=models.CASCADE, db_index=True) # 
    answer = models.BooleanField(default=False)  # which is right: T or F?
    comment = models.CharField(max_length=1000,
                               blank=True,
                               default="") # Comment if answer wrong

    def __str__(self):
        return str(self.answer)


class FillIn(models.Model):
    """Answer to a fill-in question
    """
    question = models.ForeignKey(Question, on_delete=models.CASCADE, db_index=True) 
    answer = models.CharField(max_length=FILL_IN_MAX_LENGTH) 
    comment = models.CharField(max_length=1000,
                               blank=True,
                               default="") # Comment if answer wrong

    def __str__(self):
        return self.answer


class Quiz(models.Model):
    """One quiz page
    """
    lesson_type = models.ForeignKey(LessonType, on_delete=models.CASCADE, null=True,db_index=True) 
    page_no = models.IntegerField(default=0,db_index=True)
    title = models.CharField(max_length=200, default='') 
    task = models.CharField(max_length=200, default='') # What skill this quiz page tests 
    intro_text = models.TextField(blank=True, default='') # Brief introduction to the quiz; not required 
    body_text = models.TextField(blank=True, default='') # Explanation of the required task (if present, supercedes desired_output)
    desired_output = models.TextField(blank=True, default='') # What the output should be; either this or body_text should be blank

    def __str__(self):
        return self.title


class SearchTerm(models.Model):
    """Terms to search for in answers to quiz questions
    """
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE,db_index=True) # 
    term = models.CharField(max_length=100) 
    comment = models.CharField(max_length=1000,
                               help_text="What to say if term not found") 

    def __str__(self):
        return str(self.term)


# We extend the User model with a few more fields.
class TootUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    realworld_name = models.CharField(max_length=100, default='')  # e.g. "John Smith"
    signup_datetime = models.DateTimeField(auto_now_add=True, blank=True) # auto_now_add=True is not liked by makemigrations 
    # password_reset_hash = models.CharField(max_length=128)  # hex digest of sha256
    # password_reset_initial = models.CharField(max_length=10) # first ten chars
    # password_reset_datetime = models.DateTimeField(now, blank=True) # When reset sent

    def __str__(self):
        return str(self.user)

# See https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html#onetoone
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        TootUser.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.tootuser.save(force_update=True)


# Describe which lesson a user should be shown
class TootUserLesson(models.Model):
    toot_user = models.ForeignKey(TootUser,db_index=True)
    lesson_type = models.ForeignKey(LessonType, on_delete=models.CASCADE, null=True) 
    lesson = models.ForeignKey(Lesson, null=True, db_index=True) 
    # page_no = models.IntegerField(default=0)

    class Meta:
        unique_together = (('toot_user','lesson_type'),)

    def __str__(self):
        return "Toot user # "+str(self.toot_user)+" is on lesson # "+str(self.lesson.page_no)


# Describe which video series (platform, etc) a user has chosen
class TootUserVideoSeries(models.Model):
    toot_user = models.ForeignKey(TootUser,db_index=True)
    lesson_type = models.ForeignKey(LessonType, on_delete=models.CASCADE, null=True) 
    video_series = models.ForeignKey(VideoSeries)

    class Meta:
        unique_together = (('toot_user','lesson_type'),)

    def __str__(self):
        return "Toot user # "+str(self.toot_user)+" wants video series # "+str(self.video_series)


# Describe which quiz pages a user has passed 
class TootUserPassed(models.Model):
    toot_user = models.ForeignKey(TootUser,db_index=True)
    quiz = models.ForeignKey(Quiz, null=True)  # latest quiz page

    def __str__(self):
        return "Toot user # "+str(self.toot_user)+" has passed quiz # "+str(self.quiz)

# Certificate for the user to give someone to prove they passed
class TootUserCertificates(models.Model):
    toot_user = models.ForeignKey(TootUser)
    lesson_type = models.ForeignKey(LessonType, on_delete=models.CASCADE, null=True) 
    # lesson_type = models.CharField(
    #     max_length=NUM_CHARS_IN_LESSON_TYPE,
    #     choices=LESSON_TYPES,
    #     default=LATEX,
    # )    
    certificate_id = models.CharField(max_length=36,db_index=True)  # uuid4
    certificate_datetime = models.DateTimeField(auto_now_add=True) # when issued

    def __str__(self):
        return self.certificate_id
