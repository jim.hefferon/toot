from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.db.models import Max
# from django import template

# Auth module
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate, login, logout

from lessons.models import LessonType, VideoSeries, TootUser, TootUserVideoSeries, TootUserLesson, TootUserPassed, TootUserCertificates, Quiz
from lessons.models_parameters import * # some global definitions used by multiple mods
from lessons.forms import RealNameForm, EditProfile, VideoSeriesForm, VideoSeriesFormSet

from django.conf import settings

import os, os.path, pprint
import random
import uuid  # for certificate
import re
import base64

from wand.image import Image
from wand.color import Color

from bin.run_latex import run_latex, get_error

from .models import Lesson, Question, TextResponse, MultChoice, \
  TF, FillIn, TEXT, MULT_CHOICE, TRUE_FALSE, FILL_IN

DEBUG = True
VERBOSE = True

# colors: https://color.adobe.com/create/color-wheel/?copy=true&base=2&rule=Custom&selected=0&name=Copy%20of%20Ice%20cream%20chill&mode=rgb&rgbvalues=0.611765,0.329412,0.290196,0.705882,0.745098,0.572549,0.992157,0.894118,0.733333,0.788235,0.803922,0.733333,0.886275,0.67451,0.435294&swatchOrder=0,1,2,3,4
EMPHASIZE_COLOR = "#9C544A"
BACKGROUND_COLOR = "#B4BE92"
LIGHT_COLOR = "#FDE4BB"
GRAY_COLOR = "#C9CDBB"
DARK_COLOR = "#E2AC6F"


LATEX_HTML = LESSON_TYPES_DISPLAY[LATEX] # '<span class="latex">L<sup>a</sup>T<sub>e</sub>X</span>'
TEX_HTML = LESSON_TYPES_DISPLAY[TEX] # '<span class="tex">T<sub>e</sub>X</span>'
CONTEXT_START = {'latex_html': mark_safe(LATEX_HTML),
                 'tex_html': mark_safe(TEX_HTML),
                 'errors': []}


def _html_listify(list_of_strings):
    return ["<li>{0:s}</li>".format(s) for s in list_of_strings]

def _get_toot_user(request):
    """Return the TootUser
    """
    # Get the user, see what quiz question they are on
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    if DEBUG:
        print("VIEWS.PY: request.user="+str(request.user))
        print("VIEWS.PY: request.user.toot_user="+str(request.user.__dict__))
    try:
        # toot_user = TootUser.objects.get(user__username=request.user.get_username())
        toot_user = TootUser.objects.all()
        if DEBUG:
            print("VIEWS.PY: query succeeded")
        for tu in toot_user:
            if DEBUG: 
                print("VIEWS.PY: tu="+str(tu))
    except Exception as err:
        print("VIEWS.PY: err={0:s}".format(str(err)))
    toot_user = get_object_or_404(TootUser, user=request.user)



# Default page 
# Create your views here.
# this login required decorator is to not allow to any  
# view without authenticating
# @login_required(login_url="login/")
def home(request):
    context=CONTEXT_START
    context['subject'] = ''
    info = request.GET.get('info', '')
    if info=='profileupdated':
        context['information'] = 'Your profile has been updated.'  
    return render(request, "home.html", context)
    # return HttpResponse("Hello, you're at the lessons index.")


# LaTeX lessons
DEFAULT_NUMBER_QUESTIONS = 5
def lesson(request, lesson_type, number_questions_to_ask=DEFAULT_NUMBER_QUESTIONS):
    context=CONTEXT_START
    print(">>>> lesson_type="+lesson_type)
    # Get the TootUser
    if not(request.user.is_authenticated):
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    if VERBOSE:
        print("VIEWS.PY latex(): request.user="+str(request.user))
    try:
        toot_user = TootUser.objects.get(user__username=request.user)
    except Exception as err:
        raise Http404('Unable to get the TootUser: user name={user_name}, error={err}'.format(user_name=str(request.user), err=str(err)))
    # toot_user = get_object_or_404(TootUser, user=request.user)
    print("got toot_user")
    context['choose_video_series'] = False
    context['finished_lessons'] = False
    preferred_video_series = None
    lesson = None
    # First check if they asked to advance to the next lesson
    if request.method == 'GET':  # Are they advancing to the next lesson?
        nextlesson_flag = request.GET.get('nextlesson', False)
        if nextlesson_flag:
            lesson = _user_next_lesson(toot_user, lesson_type)
            if lesson is None:
                context['finished_lessons'] = "Great!  You have finished all of the "+LESSON_TYPES_DISPLAY[lesson_type]+" lessons."
                return render(request, "lesson.html", context)
    else: # If request.method=='POST' then they are selecting a video series
        vs_fm = VideoSeriesFormSet(request.POST)
        context['vs_fm'] = vs_fm
        if not(vs_fm.is_valid()):
            context['choose_video_series'] = True
            return render(request, "lesson.html", context)
        # form is valid; save the information
        form = vs_fm[0]
        preferred_video_series = form.cleaned_data['series']
        _save_video_series_preference(toot_user, preferred_video_series)
    # end of request.method == 'POST'
    if preferred_video_series is None:
        preferred_video_series = _get_users_video_series(toot_user,lesson_type)
        if preferred_video_series is None:  # No preference, make them choose
            initial = _video_series_formset_initial(toot_user, lesson_types=[lesson_type,])
            if len(initial)==0:
                raise Http404('Unable to find any videos for the lesson type {lesson_type}'.format(lesson_type=str(lesson_type)))
            elif len(initial)==1:
                preferred_video_series = _get_video_series(lesson_type)[0]
                _save_video_series_preference(toot_user, preferred_video_series)
            else:  # ask user
                vs_fm = VideoSeriesFormSet(initial=initial)
                if VERBOSE:
                    print("toot.VIEWS.PY: lesson() vs_fm="+str(vs_fm))
                context['vs_fm'] = vs_fm
                context['choose_video_series'] = True
                return render(request, "lesson.html", context)
    # Past the next lesson and video series code.  Show the lesson.
    if lesson is None:
        lesson = _get_users_lesson(toot_user, lesson_type)
    context['lesson_type'] = lesson_type
    context['subject'] = ': '+LATEX_HTML
    context['intro_text'] = lesson.intro_text
    context['transcript'] = lesson.transcript_fn
    context['title'] = lesson.title
    context['body_text'] = lesson.body_text
    # documents
    lesson_docs = lesson.lessondocument_set.all()
    r=[]
    if lesson_docs:
        for doc in lesson_docs:
            r.append('<li> '+doc.name+' ')
            if doc.src_path:
                r.append('<a href="'+doc.src_path+'">source</a>')
            if doc.img_path:
                if doc.src_path:
                    r.append(", ")
                r.append('<a href="'+doc.img_path+'">PDF</a></li>')
        context['lesson_docs'] = '<ul>' + ''.join(r) + '</ul>'
    else:
        context['lesson_docs'] = '<p>This lesson has no documents.</p>' 
    # video 
    lesson_video = lesson.lessonvideo_set.filter(series=preferred_video_series)[0]
    context['video_file'] = lesson_video.video_url
    context['video_file_still'] = lesson_video.image_fn
    # Pick some questions at random 
    # questions = lesson.question_set.all().order_by('?')[:2]
    # 1) This is OK for small data set size but does not scale; see 
    # http://stackoverflow.com/a/6405601
    # 2) It can also pick the same question twice
    questions = lesson.question_set.all()
    size = len(questions)
    context['number_questions'] = str(size)
    # limit the number of questions asked so this cannot raise a ValueError
    number_questions_to_ask = min(number_questions_to_ask, size)
    random_indices = random.sample(range(0, size), number_questions_to_ask)
    r = []
    dex = 0
    for i in random_indices:
        # Get associated answer
        q = questions[i]
        if q.question_type == MULT_CHOICE:
            r.append('<li> '+q.question_text)
            answer = MultChoice.objects.get(question=q)
            s = [" <i>Choose one: </i>"]
            s.append(r" 1) "+answer.text_one)
            if answer.number_choices > 1:
                s.append(r" 2) "+answer.text_two)
            if answer.number_choices > 2:
                s.append(r" 3) "+answer.text_three)
            if answer.number_choices > 3:
                s.append(r" 4) "+answer.text_four)
            if answer.number_choices > 4:
                s.append(r" 5) "+answer.text_five)
            correctanswer = r"Correct choice: number "+str(answer.answer)+""
        elif q.question_type == TRUE_FALSE:
            r.append('<li><i>True or false?</i> '+q.question_text)
            answer = TF.objects.get(question=q)
            s = []
            correctanswer = ""+str(answer.answer)+""
        elif q.question_type == FILL_IN:
            r.append('<li><i>Give a short answer:</i> '+q.question_text)
            answer = FillIn.objects.get(question=questions[i])
            s = []
            correctanswer = ""+answer.answer+""
        elif q.question_type == TEXT:
            r.append('<li><i>Write the '+LATEX_HTML+':</i> '+q.question_text)
            # answer_search_terms = SearchTerms.objects.filter(question=questions[i])
            answer_text = TextResponse.objects.filter(question=questions[i])[0].text
            s = []
            correctanswer = "<pre><code>"+answer_text+"\n</code></pre>"
        s.append("""<div id="answerwrapper"
  onmouseover="document.getElementById('div{dex}').style.display = 'block';"
  onmouseout="document.getElementById('div{dex}').style.display = 'none';">
  <div id="div{dex}" style="display: none;">{correctanswer}</div></div>""".format(correctanswer=correctanswer,dex=str(dex))) # background-color: {color}
        r.append("".join(s))
        r.append(r"</li>")
        dex += 1
    context['lesson_test'] = '<ol>' + ''.join(r) + '</ol>'
    return render(request, "lesson.html", context)

def _get_users_lesson(toot_user,lesson_type):
    """Return the Lesson instance
    """
    # print("_get_users_lesson start routine: toot_user={toot_user} lesson_type={lesson_type}".format(toot_user=str(toot_user), lesson_type=pprint.pformat(lesson_type)))
    try:
        lt = LessonType.objects.get(lesson_type=lesson_type)
        tul, created = TootUserLesson.objects.get_or_create(toot_user=toot_user,
                                                            lesson_type=lt)
    except Exception as err:
        raise Http404("Database error: the system was unable to get the TootUser's lesson: toot_user={toot_user} lesson_type={lesson_type}: error {err}".format(toot_user=str(toot_user), lesson_type=str(lesson_type), err=str(err)))    
    if created:
        # print("_get_users_lesson toot_user={toot_user} lesson_type={lesson_type} new lesson created".format(toot_user=str(toot_user), lesson_type=pprint.pformat(lesson_type)))
        try:
            lesson = Lesson.objects.get(lesson_type=lt,
                                        page_no=1)
        except Exception as err:
            raise Http404("Database error: the system was unable to find the TootUser's lesson as the first lesson: toot_user={toot_user} lesson_type={lesson_type}: error {err}".format(toot_user=str(toot_user), lesson_type=str(lesson_type), err=str(err)))    
        try:
            tul.lesson = lesson
            tul.save()
        except Exception as err:
            raise Http404("Database error: the system was unable to set the TootUser's lesson to the first lesson: toot_user={toot_user} lesson_type={lesson_type}: error {err}".format(toot_user=str(toot_user), lesson_type=str(lesson_type), err=str(err)))    
    else:
        lesson = tul.lesson
    return lesson

def _user_next_lesson(toot_user,lesson_type):
    """Return the Lesson instance or None if all the lessons are done
    """
    # print("_user_next_lesson start routine: toot_user={toot_user} lesson_type={lesson_type}".format(toot_user=str(toot_user), lesson_type=pprint.pformat(lesson_type)))
    current_lesson = _get_users_lesson(toot_user,lesson_type)
    # print("_user_next_lesson start routine: current_lesson={current_lesson}".format(current_lesson=pprint.pformat(current_lesson)))
    # print("_user_next_lesson start routine: page_no={page_no}".format(page_no=str(current_lesson.page_no)))
    next_page_no = (current_lesson.page_no)+1
    try:
        lsn = Lesson.objects.get(lesson_type__lesson_type=lesson_type,
                                 page_no=next_page_no)
    except Lesson.DoesNotExist:
        return None
    else:
        try:
            tul, created = TootUserLesson.objects.get_or_create(toot_user=toot_user,
                                                                lesson_type__lesson_type=lesson_type)
            tul.lesson=lsn
            tul.save()
        except Exception as err:
            raise Http404("Database error: the system was unable to save the TootUser's lesson: toot_user={toot_user} lesson_type={lesson_type} lesson={lesson}: error {err}".format(toot_user=str(toot_user), lesson_type=str(lesson_type), lesson=str(lsn), err=str(err)))    
        return lsn

def _get_users_video_series(toot_user,lesson_type):
    """Return the VideoSeries or None
    """
    try:
        tuvs = TootUserVideoSeries.objects.get(toot_user=toot_user,
                                               video_series__lesson_type__lesson_type=lesson_type)
        return tuvs.video_series
    except TootUserVideoSeries.DoesNotExist:
        return None
    except Exception as err:
        raise Http404("Database error: the system was unable to get the TootUser's video series: toot_user={toot_user} lesson_type={lesson_type}: error {err}".format(toot_user=str(toot_user), lesson_type=str(lesson_type), err=str(err)))         

# def _get_users_video_series_form(toot_user,lesson_type):
#     """Return the form, or None.  Returns None if there is only one VideoSeries
#     and so the system just sets that to be the user's preference.
#     """
#     try:
#         vs = VideoSeries.objects.filter(lesson_type__lesson_type=lesson_type)
#     except VideoSeries.DoesNotExist:
#         raise Http404("Database error: no video series exists for the lesson type: toot_user={toot_user} lesson_type={lesson_type}".format(toot_user=str(toot_user), lesson_type=str(lesson_type)))         
#     except Exception as err:
#         raise Http404("Database error: the system was unable to get the TootUser's video series: toot_user={toot_user} lesson_type={lesson_type}: error {err} page_no={1:s}".format(toot_user=str(toot_user), lesson_type=str(lesson_type), err=str(err)))
#     # If there is only one choice, just set it.
#     if (len(vs)==1):
#         _save_video_series_preference(toot_user, vs[0])
#         return None
#     else: # User must choose, so make up a form and send it.
#         choices = [(str(x.id),x.description) for x in vs]
#         users_pref = _get_users_video_series(toot_user,lesson_type)
#         return VideoSeriesForm(lesson_type=lesson_type,
#                                choices=choices,
#                                initial=users_pref)

def _get_video_series(lesson_type):
    """Return a list (a queryset) of VideoSeries instances, or None.
    """
    try:
        vs = VideoSeries.objects.filter(lesson_type__lesson_type=lesson_type)
    except VideoSeries.DoesNotExist:
        raise Http404("Database error: no video series exists for the lesson type lesson_type={lesson_type}".format(lesson_type=str(lesson_type)))         
    except Exception as err:
        raise Http404("Database error: unable to get the list of video series for lesson_type={lesson_type}: error={err}".format(lesson_type=str(lesson_type), err=str(err)))
    return vs

def _save_video_series_preference(toot_user, preferred_video_series):
    """Save the user's preference
      toot_user TootUser instance
      preferred_video_series VideoSeries instance
    """
    lesson_type = preferred_video_series.lesson_type
    try:
        tuvs = TootUserVideoSeries.objects.get(toot_user=toot_user,
                                               lesson_type = lesson_type)
    except TootUserVideoSeries.DoesNotExist:
        tuvs = TootUserVideoSeries(toot_user=toot_user,
                                   lesson_type = lesson_type)
    except Exception as err:
        raise Http404("Database error: unable to create the TootUser's video series preference: toot_user={toot_user} video_series={video_series}: error {err}".format(toot_user=str(toot_user), lesson_type=str(lesson_type), video_series=str(video_series), err=str(err)))
    try:
        tuvs.video_series = preferred_video_series
        tuvs.save()
    except Exception as err:
        raise Http404("Database error: unable to save the TootUser's video series preference: toot_user={toot_user} video_series={video_series}: error={err}".format(toot_user=str(toot_user), lesson_type=str(lesson_type), video_series=str(video_series), err=str(err)))



# Results
def latex_results(request):
    context={}
    return render(request, "results.html", context)

# Quiz for certification
def quiz(request, quiz_id, lesson_type=LATEX):    
    # Set the variables common to GET and POST
    context=CONTEXT_START
    context['subject'] = ': '+LATEX_HTML
    context['title'] = 'Certification quiz'
    # Get the user, see what quiz question they are on
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    if DEBUG:
        print("VIEWS.PY: request.user="+str(request.user))
        print("VIEWS.PY: request.user.toot_user="+str(request.user.__dict__))
    try:
        # toot_user = TootUser.objects.get(user__username=request.user.get_username())
        toot_user = TootUser.objects.all()
        if DEBUG:
            print("VIEWS.PY: query succeeded")
        for tu in toot_user:
            if DEBUG: 
                print("VIEWS.PY: tu="+str(tu))
    except Exception as err:
        print("VIEWS.PY: err={0:s}".format(str(err)))
    toot_user = get_object_or_404(TootUser, user=request.user)
    # print("VIEWS.PY: toot_user="+str(toot_user))
    quiz = None
    # See if user is done
    try:
        tuc = TootUserCertificate.objects.get(toot_user=toot_user,
                                              quiz__lesson_type=lesson_type)
    except:
        pass
    else:
        return redirect('/lessons/{0:s}/quiz/done'.format(lesson_type),msg='You have graduated.')  # TODO: make a quiz done page
    # User not done so get page number
    try:
        toot_user_passed = TootUserPassed.objects.get(toot_user=toot_user,
                                                      quiz__lesson_type=lesson_type)
    except:  # If initial quiz for this user, make that record
        try:
            quiz = Quiz.objects.get(lesson_type=lesson_type,page_no=1)
        except:
            raise Http404("Database error: the systm was unable to find the first quiz: lesson_type={0:s} page_no={1:s}".format(lesson_type,str(1)))         
        try:
            toot_user_passed = TootUserPassed(toot_user=toot_user,
                                              quiz=quiz).save()
        except Exception as err:
            raise Http404("Database error: the system was unable to find which is the right quiz for this user: toot_user={0:s} quiz={1:s}: error={2:s}".format(str(toot_user),str(quiz),str(err))) 
    if quiz is None:
        quiz = toot_user_passed.quiz
    context['page_no'] = quiz.page_no
    # Fetch data common to GET and POST pages 
    context['title'] = quiz.title
    context['task'] = quiz.task
    context['intro_text'] = quiz.intro_text
    if quiz.body_text:
        context['body_text'] = quiz.body_text
    else:
        context['body_text'] = """<p>
  Enter the """+LATEX_HTML+""" source to produce the output highlighted below, 
  and then press <i>Submit</i>.
  Your source must have a preamble as well as a document body.
  </p>

<p id="required_input">
"""+quiz.desired_output+"""
</p>
"""
    # POST means sourcefile submitted, GET means it is solicited
    if request.method == 'GET':
        pass
    elif request.method == 'POST':
        # username = request.user.username
        # if not(request.user.username):
        #     context['error'] = "The system cannot find your user name."
        #     render(request, "quiz.html", context)
        sourcefile = request.POST.get('sourcefile','')
        context['sourcefile'] = sourcefile
        if not(sourcefile):
            context['error'] = "The system cannot find the source file."
            render(request, "quiz.html", context)
        # Look for errors before running LaTeX
        searchterms = []
        try:
            sts = quiz.searchterm_set.all()
            searchterms = [(st.term,st.comment) for st in sts]
        except:
            context['error'] = 'There was a database error when checking the response.  Sorry.'
            return render(request, "quiz.html", context)
        error_dct = test_quiz_doc(sourcefile,searchterms)
        if error_dct['error_flag']:
            context['errors'] = error_dct
            return render(request, "quiz.html", context)
        # Run the sourcefile
        r = run_quiz_doc(sourcefile,context)
        error_dct['run_errors'] = r['errors']
        context['errors'] = error_dct
        context['png'] = r['png']
        # If success, update the user's quiz page
        if not(r['error_flag']):
            next_quiz_page_no = quiz.page_no+1
            try:
                toot_user_passed,created = TootUserPassed(toot_user=toot_user,
                                                          quiz__lesson_type=lesson_type).update_or_create(quiz=Quiz.get(lesson_type=lesson_type,                                                     page_no=next_quiz_page_no))
            except Quiz.DoesNotExist: 
                cert_id = make_certificate_id()
                tuc = TootUserCertificates(toot_user=toot_user,
                                           lesson_type=lesson_type,
                                           certificate_id = cert_id).create()
                return redirect('/lessons/{0:s}/quiz/done'.format(lesson_type),msg='You have graduated.')  # TODO: make a quiz done page
            except Exception as err:
                raise Http404("Database error: the system was unable to make this user as having finished the quizzes: {0:s}.".format(str(err))) 
    return render(request, "quiz.html", context)

# def get_img_fn(extension='.png'):
#     """Return a name for the image file.
#       extension  graphic file extension
#     """
#     return uuid.uuid4().hex+extension

# def get_file_loc(fn, d=settings.DYNAMIC_IMG_DIR):
#     """Return full path to the file.
#       fn name of the file
#       d directory
#     """
#     return os.path.join(d,fn)

def make_certificate_id():
    """Return a unique identifier that fits in the 36 chars allocated in the db.
    """
    return str(uuid.uuid4())

def convert_pdf(s, d=settings.DYNAMIC_IMG_DIR, extension='.png'):
    """Convert the PDF file to a .png
      s = PDF file as a string
      d = dir into which the image is placed
    Returns the image in a form suitable for embedding in a HTML page as
    "<a src='put the string here' ..>". 
    """
    # print("convert_pdf: s? "+str(bool(s)))
    img = Image(blob=s, resolution=96*4)    
    img.background_color = Color('white') 
    img.alpha_channel = 'remove'    # replace transparency with bg since some mobiles have black background
    # fn = get_img_fn(extension=extension)
    # try:
    #     # img.save(filename=get_file_loc(fn,d=d))
    #     img.save(filename="/home/ftpmaint/src/toot/toot/tmp/fn.png")
    # except Exception as err:
    #     print("EXCEPTION IMAGE: {0!s}".format(err))
    return embedded_image(img)

# From http://stackoverflow.com/a/7389616/7168267
def embedded_image(img, image_type = 'png'):
    """Convert the image into a format suitable for embedding in HTML 
    <a src="..."> 
      img  instance of Wand's Image 
    """
    data_uri = base64.b64encode(img.make_blob(image_type)).decode('utf-8').replace('\n', '')
    return "data:image/"+image_type+";base64,{0}".format(data_uri)

def run_quiz_doc(sourcefile, context):
    """Run the source file through LaTeX
    """
    r={'error_flag': False,
        'errors':[], 
       'pdf':None, 
       'png':None}  # returned value
    rc,pdf,log = run_latex(sourcefile)
    # print("RUN_QUIZ_DOC: rc="+str(rc)+" PDF? "+str(bool(pdf)))
    if DEBUG:
        r['rc'] = str(rc) # for debugging
        r['pdf'] = pdf    # for debugging
        r['log'] = log    # for debugging
    if rc == 0:
        if pdf:
            try:
                # print("RUN_QUIZ_DOC: about to get a png")
                r['png'] = convert_pdf(pdf)
                # print("RUN_QUIZ_DOC: got a png")
            except:
                r['errors'].append(mark_safe('The PDF was created but there was a failure in converting it to the PNG format.'))
                r['error_flag'] = True
        else:
            r['errors'].append(mark_safe('Running the source file did not generate an error but also did not generate a PDF.'))
            r['error_flag'] = True
    else:
        r['error_flag'] = True
        if rc > 127:
            r['errors'].append(mark_safe("Your document is too big or takes too long so it was shut down by the system."))
        else:
            error_dct = get_error(log)
            c = ["Line {line!s} produced the error message ".format(line=escape(error_dct.get('line','--unknown--'))),
                 "<code>{text!s}</code>. ".format(text=escape(error_dct.get('text','--unknown--'))),
                 "The contents of that line is: <code>{code!s}</code>.".format(code=escape(error_dct.get('code','--unknown--')))]
            r['errors'].append("".join(c))
    return r

documentclass_line = re.compile(r"\s*\\documentclass{article}",re.VERBOSE)
begindocument_line = re.compile(r"^\s*\\begin{document}",re.MULTILINE|re.VERBOSE)
enddocument_line = re.compile(r"^\s*\\end{document}",re.MULTILINE|re.VERBOSE)
inputenc_line = re.compile(r"^\s*\\usepackage\[utf8\]{inputenc}",re.MULTILINE|re.VERBOSE)
amsmath_line = re.compile(r"^\s*\\usepackage{amsmath",re.MULTILINE|re.VERBOSE)
def _append_safestring(r,s):
    r.append(mark_safe(s))
def test_any_doc(s):
    """Some checks that any LaTeX document should pass.
    """
    errors = []
    warnings = []
    # First line a documentclass?
    m = documentclass_line.match(s)
    if not(m):
        _append_safestring(errors,r"The first line should be <code>\documentclass{article}</code>.")
    # Begin and end?
    m = begindocument_line.search(s)
    if not(m):
         _append_safestring(errors,r"Every document needs a <code>\begin{document}</code> line.")
    m = enddocument_line.search(s)
    if not(m):
         _append_safestring(errors,r"Every document needs a <code>\end{document}</code> line.")
    m = amsmath_line.search(s)
    if not(m):
         _append_safestring(errors,r"Put a line in the preamble saying <code>\usepackage{amsmath,amssymb,amsthm}</code>.")
    # Warnings
    m = inputenc_line.search(s)
    if not(m):
         _append_safestring(warnings,r"Put a line in the preamble saying <code>\usepackage[utf8]{inputenc}</code>.")
    return errors, warnings

def test_quiz_doc(sourcefile,searchterms):
    """Look through the file for errors.  
      sourcefile  LaTeX text file
      searchterms  list of pairs (term,comment if that term not found)
    Return a dictionary.
    """
    # print("TEST_QUIZ_DOC: searchterms={0:s}".format(pprint.pformat(searchterms)))
    r={'error_flag':False, 
       'errormsg': '',  # any doc must pass this
       'warningmsg': '',  # warn about things any doc should have
       'commentsmsg': ''}  # applies to this specific quiz doc
    # Any quiz's source document
    r['errormsg'], r['warningmsg'] = test_any_doc(sourcefile)
    if r['errormsg']:
        r['error_flag'] = True
    # This quiz's source document
    r['commentsmsg'] = []
    # print("TEST_QUIZ_DOC: sourcefile={0:s}".format(pprint.pformat(sourcefile)))
    for (term,comment) in searchterms:
        print("TEST_QUIZ_DOC: term={0:s}".format(pprint.pformat(term)))
        m = re.search(term,sourcefile,re.MULTILINE)
        if not(m):
            print("TEST_QUIZ_DOC: no match")
            r['commentsmsg'].append(comment)
    if r['commentsmsg']:
        r['error_flag'] = True
    print("TEST_QUIZ_DOC: r={0:s}".format(pprint.pformat(r)))
    return r


# ==============================================
# Quiz for certification
DEFAULT_BODY = """
  <p>
    Enter the """+LATEX_HTML+""" source to produce the output highlighted 
    below and then press <i>Submit</i>.
    Your source must have a preamble as well as a document body.
    </p>

  <p id="required_input">{0:s}
    </p>
"""

def quiz_refactored(request, lesson_type=None):
    """Return the quiz page.
    """
    print("QUIZ_REFACTORED: lesson_type={0:s}".format(str(lesson_type)))
    if not(lesson_type in LESSON_TYPE_SET):
        raise Http404('No appropriate lesson type found for the quiz')
    # Set a couple of context variables, for any error pages
    context=CONTEXT_START
    context['subject'] = ': '+LESSON_TYPES_DISPLAY[lesson_type]
    # Identify the user
    if not(request.user.is_authenticated):
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    if VERBOSE:
        print("VIEWS.PY: request.user="+str(request.user))
    toot_user = get_object_or_404(TootUser, user=request.user)
    # Are there any quizzes of the requested type?
    max_quiz_page_no = _get_max_quiz_page_no(lesson_type)
    if (max_quiz_page_no == 0):
        raise Http404('No quiz for this lesson type exists. lesson_type={0:s}'.format(lesson_type))
    # If this user is done all available quizzes, give them a certificate
    #  (even if they are visiting later)
    if _has_finished(toot_user,lesson_type):
        return redirect('certificate/')
    # What page is the user now on?
    quiz = _get_current_quiz(toot_user,lesson_type)
    # If GET, user is requesting a page.  If POST, user has sent a reply.
    if request.method == 'GET':
        print("request.method==GET")
        context['sourcefile'] = ''
    elif request.method == 'POST':
        print("request.method==POST")
        sourcefile = request.POST.get('sourcefile','')
        print("QUIZ_REFACTORED: sourcefile={0:s}".format(str(sourcefile)))
        if not(sourcefile): # TODO: add to form.errors
            context['error'] = "The system cannot find the source file.  Did you enter anything in the text box?"
            render(request, "quiz.html", context)
        context['sourcefile'] = sourcefile
        # Look for errors before running LaTeX
        searchterms = []
        try:
            sts = quiz.searchterm_set.all()
            searchterms = [(st.term,st.comment) for st in sts]
        except:
            raise Http404('Database error: unable to fetch search terms when checking the response')
        error_dct = test_quiz_doc(sourcefile,searchterms)
        if error_dct['error_flag']:
            context['errors'] = error_dct
            context = _get_context_from_quiz(context,quiz)
            return render(request, "quiz.html", context)
        # Run the sourcefile
        r = run_quiz_doc(sourcefile,context)
        error_dct['run_errors'] = r['errors']
        context['errors'] = error_dct
        context['png'] = r['png']
        # If success, update the user's quiz page
        if not(r['error_flag']):
            _make_passed(toot_user,quiz)
            context['sourcefile'] = ''
            # If this was the last page, make a certificate for picking up
            if (_get_max_quiz_page_no(lesson_type)< quiz.page_no+1):
                _make_certificate(toot_user,lesson_type)
                return redirect('certificate/')
            else:  # get context info for new quiz
                quiz = _get_current_quiz(toot_user,lesson_type)
    # Get context information for the page
    context = _get_context_from_quiz(context,quiz)
    return render(request, "quiz.html", context)

def _get_context_from_quiz(context,quiz):
    context['page_no'] = quiz.page_no
    context['title'] = quiz.title
    context['task'] = quiz.task
    context['intro_text'] = quiz.intro_text
    if quiz.body_text:
        context['body_text'] = quiz.body_text
    else:
        context['body_text'] = DEFAULT_BODY.format(quiz.desired_output)
    return context

def _get_current_quiz(toot_user,lesson_type):
    """Return the quiz that the user should take now.
    """
    # Case 1) Get the toot_user_passes instance from the db
    try:
        toot_user_passed = TootUserPassed.objects.get(toot_user=toot_user,
                                                      quiz__lesson_type=lesson_type)
        old_quiz = toot_user_passed.quiz
    except Exception as err:
        print("lesson.views _get_current_quiz(): unable to get toot_user_passed: toot_user={0:s} lesson_type={1:s} Error: {2:s}".format(str(toot_user),str(lesson_type),str(err)))
        # Case 2) No quizzes passed yet, grab the first
        try:
            return Quiz.objects.get(lesson_type=lesson_type,page_no=1)
        except:
            raise Http404("Database error: the system was unable to get the first quiz: lesson_type={0:s} page_no={1:s}".format(lesson_type,str(1))) 
    # Get the next quiz in the sequence.
    next_page_no = old_quiz.page_no+1
    try:
        quiz = Quiz.objects.get(lesson_type=lesson_type,
                                page_no=next_page_no)
    except Exception as err:
        raise Http404("Database error: the system was unable to find the next quiz page of this type: toot_user={0:s} lesson_type={1:s} next_quiz page number={2:s}: error={3:s}".format(str(toot_user),str(lesson_type),str(next_page_no),str(err))) 
    return quiz

def _get_max_quiz_page_no(lesson_type):
    """Return the largest existing quiz page number for this type of quiz.
     Returns 0 if there are none.
    """
    try:
        q = Quiz.objects.filter(lesson_type=lesson_type)
    except:
        raise Http404("Database error: the system was unable to find quizzes for this lesson type: lesson_type={0:s}".format(lesson_type))
    if not(q.exists()):
        return 0
    try:
        return q.aggregate(Max('page_no'))['page_no__max']
    except:
        raise Http404("Database error: the system was unable to find the final quiz number: lesson_type={0:s}".format(lesson_type))         
  
def _has_finished(toot_user,lesson_type):
    """Return True if the user has already finished all questions in this quiz.
    """
    try:
        tuc = TootUserCertificate(toot_user=toot_user,lesson_type=lesson_type).get()
    except:
        return False
    return True

def _make_certificate(toot_user,lesson_type):
    """Mark that the user has finished this quiz.
    """
    try:
        cert_id = make_certificate_id()
        tuc = TootUserCertificates(toot_user=toot_user,
                                   lesson_type=lesson_type,
                                   certificate_id = cert_id)
        tuc.save()
    except:
        raise Http404('Database error: unable to make certificate. user={0:s}, lesson_type={0:s}'.format(str(toot_user),lesson_type))

def _make_passed(toot_user,quiz):
    """Mark that the user has passed this quiz.
    """
    # These two are useful for error messages
    lesson_type = quiz.lesson_type
    current_page_no = quiz.page_no
    if (current_page_no == 1):  # make an initial record
        try:
            tup = TootUserPassed(toot_user=toot_user,
                                 quiz=quiz)
        except Exception as err:
            raise Http404('Database error: unable to make this user as having passed this first quiz. user={0:s}, lesson_type={1:s}, page_no={2:s} error={3:s}'.format(str(toot_user),str(lesson_type),str(current_page_no),str(err)))
    else:    # update the existing record
        try:
            tup = TootUserPassed.objects.get(toot_user=toot_user,
                                             quiz__lesson_type=lesson_type)
            tup.quiz = quiz
        except Exception as err:  
            raise Http404('Database error: unable to make this user as having passed this quiz page. user={0:s}, lesson_type={1:s}, page_no={2:s} error={3:s}'.format(str(toot_user),str(lesson_type),str(current_page_no),str(err)))
    try:
        tup.save()
    except Exception as err:  
        raise Http404('Database error: unable to save that this user has passed this quiz page. user={0:s}, lesson_type={1:s}, page_no={2:s} error={3:s}'.format(str(toot_user),str(lesson_type),str(current_page_no),str(err)))




# ==============================
def certificate(request, lesson_type=None):
    """Certify the user passed the quizzes for this lesson.
    """
    if VERBOSE and DEBUG:
        print("CERTIFICATE: lesson_type={0:s}".format(str(lesson_type)))
        print("CERTIFICATE: request.method={0:s}".format(str(request.method)))
        print("CERTIFICATE: request.GET={0:s}".format(pprint.pformat(str(request.GET))))
    context=CONTEXT_START
    # Are they checking an ID?
    if ((request.method == 'GET')
        and ('id' in request.GET)):
        id = request.GET.get('id',None)
        if not(id):
            raise Http404('You must furnish a certificate id.')
        try:
            tuc = TootUserCertificates.objects.get(certificate_id=id)
        except TootUserCertificates.NotFound:
            context['result'] = 'The certificate id is not in the database.'
            return render(request, "certificate.html", context)
        except Exception as err:
            raise Http404('Database error: unable to get the TootUserCertificates: lesson_type={0:s}, id={1:s} error={2:s}'.format(str(lesson_type),str(id),str(err)))
        return _issue_certificate(request,context,tuc, certify_or_verify='verifies')
    # Below here, visitor is not checking on an ID
    if not(lesson_type in LESSON_TYPE_SET):
        raise Http404('You cannot get a certificate in that')
    # Set a couple of context variables, for any error pages
    context=CONTEXT_START
    context['subject'] = ': '+LESSON_TYPES_DISPLAY[lesson_type]
    # Identify the user
    if not(request.user.is_authenticated):
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    if VERBOSE:
        print("VIEWS.PY: certificate() request.user="+str(request.user))
    toot_user = get_object_or_404(TootUser, user=request.user)
    # Verify certificate exists for this user
    try:
        tuc = TootUserCertificates.objects.get(toot_user=toot_user,
                                               lesson_type=lesson_type)
    except TootUserCertificates.DoesNotExist:
        context['errors'].append('The database does not show that you have finished the quizzes for this subject.')
        return render(request, "certificate.html", context)
    except Exception as err:
        raise Http404('Database error: unable to get the requested TootUserCertificates: toot_user={0:s}, lesson_type={1:s}, error={2:s}'.format(str(toot_user),str(lesson_type),str(err)))
    # See if user has a real name
    name_string = tuc.toot_user.realworld_name
    if ((name_string is None)
        or not(name_string.strip())):
        if request.method == 'GET':
            # send form
            context['form'] = RealNameForm()
            context['get_user_name'] = True
            return render(request, "certificate.html", context)
        if (request.method == 'POST'):
            # check form
            form = RealNameForm(request.POST)
            context['form'] = form
            if form.is_valid():
                context['get_user_name'] = False
                try:
                    new_name = form.cleaned_data['your_name']
                    toot_user.realworld_name = new_name
                    toot_user.save(force_update=True)
                except Exception as err:
                    raise Http404('Database error: unable the TootUser: toot_user={0:s}, new name={1:s}, error={2:s}'.format(str(toot_user),str(new_name),str(err)))
            else:
                context['get_user_name'] = True
                return render(request, "certificate.html", context)
    # Issue certificate
    return _issue_certificate(request, context, tuc)

def _issue_certificate(request, context, tuc, certify_or_verify = 'certifies'):
    """Return the rendered certificate
    """
    context['certify_or_verify'] = certify_or_verify
    context['lesson_type'] = tuc.lesson_type
    context['lesson_type_string'] = LESSON_TYPES_DISPLAY[tuc.lesson_type]
    context['name_string'] = tuc.toot_user.realworld_name
    context['certificate_id'] = tuc.certificate_id
    return render(request, "certificate.html", context)


# ========================================
# Allow user to edit real world name, video series

def get_video_series(subject):
    """Get all VideoSeries for this subject
    """
    return VideoSeries.objects.filter(lesson_type__lesson_type=subject)

def get_all_video_series():
    """Get all the VideoSeries, indexed by subject
    """
    vs_dct = {}    
    try:
        vs_qs = VideoSeries.objects.all()
    except Exception as err:
        raise Http404("Database error: unable to get the video series: {err}".format(err=str(err)))
    for vs in vs_qs:
        subject = vs.lesson_type.lesson_type
        if not(subject in vs_dct):
            vs_dct[subject] = []
        vs_dct['subject'].append(vs)
    return vs_dct
        
# def get_video_series_form(video_series,preference=None):
#     """Return form to choose a video series.
#       video_series  list of VideoSeries.objects for this form
#       preference  integer or None, id of the preferred series
#     """
#     choices = [(str(vs.id),vs.description) for vs in video_series]
#     # print("lessons.views.get_video_series_form choices="+pprint.pformat(choices))
#     init_dct = {'choices':choices}
#     if preference is None:
#         initial = None
#     else:
#         initial = str(preference)
#     vsf = VideoSeriesForm(choices=choices,initial=initial)
#     # print("vsf="+str(vsf))
#     return vsf

def get_video_series_form(toot_user,subject=LATEX,bind=False,post=None):
    """Return form to choose a video series.
      video_series  list of VideoSeries.objects for this form
      preference  integer or None, id of the preferred series
    """
    if bind:
        return VideoSeriesForm(post,lesson_type=subject)
    else:
        print("views.get_video_series_form "+"about to get users video series form toot_user={toot_user} subject={subject}".format(toot_user=str(toot_user), subject=str(subject)))
        video_series = _get_users_video_series(toot_user, subject)
        if video_series is None:
            initial = {}
        else:
            initial = {'series': video_series.pk}
        return VideoSeriesForm(lesson_type=subject, initial=initial)


def get_video_series_formset(toot_user, lesson_types=None):
    """Return a form set for the user to choose the VideoSeries's.
    It is initialized with the user's preferences.
      toot_user  instance of TootUser
      lesson_types=None  list (or None) of strings that are 
         LessonType.lesson_type's.  If present, the returned
         formset will consist of forms for these lessons, in the order given.  
         If None, the formset will consist of forms for all active 
         LessonType's, ordered by 'series'.
    Remark: lesson_types=None involves hitting the dB another time so
    if you can avoid it, great.
    """
    print("lessons.views.get_video_series_formset_initial start")
    error_prefix = "Error in get_video_series_formset:"
    # Get this user's preferences
    try:
        tuvs = TootUserVideoSeries.objects.filter(toot_user=toot_user)
    except Exception as err:
        raise Http404("{error_prefix} Database error: unable to get the TootUserVideoSeries for the user toot_user={toot_user}: error={err}".format(error_prefix=error_prefix, toot_user=str(toot_user), err=str(err)))
    # Turn it into a mapping: lesson_type  ->  video_series
    prefs_dct = {}  # lesson_type -> VideoSeries 
    for u in tuvs:
        prefs_dct[u.video_series__lesson_type__lesson_type] = u.video_series
    if VERBOSE: 
        print("lessons.views.get_video_series_forms prefs_dct="+pprint.pformat(prefs_dct))
    # In the lesson_types=None case, get a list of series that are available
    try:
        active_lesson_types = LessonType.objects.filter(available=True).order_by('lesson_type')
        lesson_types = [x.lesson_type for x in active_lesson_types]
    except Exception as err:
        raise Http404("{error_prefix} Database error: unable to get the list of active video series: error={err}".format(error_prefix=error_prefix, err=str(err)))
    # Get list of initial setting for formset.
    initial=[]
    for lt in lesson_types:
        d={'lesson_type':lt}
        if lt in prefs_dct:
            d['series'] = str(prefs_dct[lt])
        initial.append(d)
    # Make the formset
    return VideoSeriesFormSet(initial=initial)


def _video_series_formset_initial(toot_user, lesson_types=None):
    """Return a form set for the user to choose the VideoSeries's.
    It is initialized with the user's preferences.
      toot_user  instance of TootUser
      lesson_types=None  list (or None) of strings that are 
         LessonType.lesson_type's.  If present, the returned
         formset will consist of forms for these lessons, in the order given.  
         If None, the formset will consist of forms for all active 
         LessonType's, ordered by 'series'.
    Remark: lesson_types=None involves hitting the dB another time so
    if you can avoid it, great.
    """
    print("lessons.views._video_series_formset_initial start")
    error_prefix = "Error in_video_series_formset_initial:"
    # Get this user's preferences
    try:
        tuvs = TootUserVideoSeries.objects.filter(toot_user=toot_user)
    except Exception as err:
        raise Http404("{error_prefix} Database error: unable to get the TootUserVideoSeries for the user toot_user={toot_user}: error={err}".format(error_prefix=error_prefix, toot_user=str(toot_user), err=str(err)))
    # Turn it into a mapping: lesson_type  ->  video_series
    prefs_dct = {}  # lesson_type -> VideoSeries 
    for u in tuvs:
        print(">>> u.video_series.lesson_type.lesson_type="+str(u.video_series.lesson_type.lesson_type))
        prefs_dct[u.video_series.lesson_type.lesson_type] = u.video_series
    if VERBOSE: 
        print("lessons.views.get_video_series_forms prefs_dct="+pprint.pformat(prefs_dct))
    # In the lesson_types=None case, get a list of series that are available
    try:
        active_lesson_types = LessonType.objects.filter(available=True).order_by('lesson_type')
        lesson_types = [x.lesson_type for x in active_lesson_types]
    except Exception as err:
        raise Http404("{error_prefix} Database error: unable to get the list of active video series: error={err}".format(error_prefix=error_prefix, err=str(err)))
    # Get list of initial setting for formset.
    initial=[]
    for lt in lesson_types:
        d={'lesson_type':lt}
        if lt in prefs_dct:
            d['series'] = str(prefs_dct[lt].pk)
        initial.append(d)
    # Finish
    return initial


def update_profile(request):
    context=CONTEXT_START
    error_prefix = "toot.views.py update_profile()"
    if VERBOSE:
        print("{error_prefix} request.method={request_method}".format(error_prefix=error_prefix, request_method=str(request.method)))
    # Identify the user
    if not(request.user.is_authenticated):
        if VERBOSE:
            print("toot.VIEWS.PY: update_profile() user is not authenticated")
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    if VERBOSE:
        print("toot.VIEWS.PY: update_profile() request.user="+str(request.user))
    toot_user = get_object_or_404(TootUser, user=request.user)
    # Get the data
    if request.method == 'GET':
        profile_fm = EditProfile(initial={'email':toot_user.user.email,'realworld_name':toot_user.realworld_name})
        context['profile_fm'] =  profile_fm
        initial = _video_series_formset_initial(toot_user, lesson_types=None)
        # vs_fm = VideoSeriesFormSet(initial)
        vs_fm = VideoSeriesFormSet(initial=initial)
        if VERBOSE:
            print("toot.VIEWS.PY: update_profile() initial="+pprint.pformat(initial))
            print("toot.VIEWS.PY: update_profile() vs_fm="+str(vs_fm))
        context['vs_fm'] = vs_fm
        return render(request,'registration/profile_change.html',context)
    else:  # if request.method == 'POST':
        if VERBOSE:
            for k in request.POST:
                print("lessons.views update_profile() k={0:s} v={1:s}".format(str(k),str(request.POST[k])))
        profile_fm = EditProfile(request.POST)
        context['profile_fm'] =  profile_fm
        # vs_fm = get_video_series_formset(toot_user, lesson_types=None)    
        vs_fm = VideoSeriesFormSet(request.POST)
        context['vs_fm'] = vs_fm
        if (profile_fm.is_valid()
            and vs_fm.is_valid()):
            _save_profile(toot_user, profile_fm, vs_fm)
            return HttpResponseRedirect('/lessons/?info=profileupdated')
        else:
            return render(request,'registration/profile_change.html',context)

def _save_profile(toot_user, profile_fm, vs_fm):
    """Save the profile data to the database.
    """
    error_prefix = "Trouble saving profile while updating profile: "
    try:
        profile_fm.save(toot_user)
    except Exception as err:
        raise Http404("{error_prefix} Database error: unable to save the profile form: error={err}".format(error_prefix=error_prefix, err=str(err)))
    for fm in vs_fm: 
        preferred_video_series = fm.cleaned_data['series']
        print("_save_profile: preferred_video_series="+str(preferred_video_series))
        _save_video_series_preference(toot_user, preferred_video_series)
            

