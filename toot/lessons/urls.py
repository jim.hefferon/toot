from django.conf.urls import url
from django.contrib.auth import views as auth_views

from lessons.models_parameters import * # some global definitions used by multiple mods
from . import views

urlpatterns = [
    url(r'^profile/$', views.update_profile, name='profile'),
    url(r'^$', views.home, name='home'),
    url(r'^latex/results/$', views.latex_results, name='latex_results'),
    url(r'^certificate/(?P<lesson_type>[\w]{{1,{0:s}}})/$'.format(str(NUM_CHARS_IN_LESSON_TYPE)), views.certificate, name='certificate'),
    # url(r'^latex/(?P<lesson_id>[0-9]+)/$', views.latex, name='latex'),
    url(r'^quiz/(?P<lesson_type>[a-z]{{1,{0:s}}})/$'.format(str(NUM_CHARS_IN_LESSON_TYPE)), views.quiz_refactored, name='quiz'),
    url(r'^(?P<lesson_type>[\w]{{1,{0:s}}})/$'.format(str(NUM_CHARS_IN_LESSON_TYPE)), views.lesson, name='lesson_page'),
]
