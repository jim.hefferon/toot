#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Read the questions in, as XML.
"""
__version__ = '0.9.5'
__author__ = 'Jim Hefferon'
__license__ = 'GPL 3'

import sys, os, os.path, re, pprint, argparse, traceback, time

SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))  # location of this script
LESSONS_REL_DIR = "../lessons"
LESSONS_DIR = os.path.normpath(os.path.join(SCRIPT_DIR, LESSONS_REL_DIR))
import importlib.machinery # see http://stackoverflow.com/a/29855240/7168267
models_parameters = importlib.machinery.SourceFileLoader('models_parameters',os.path.normpath(os.path.join(LESSONS_DIR,'models_parameters.py'))).load_module()
from models_parameters import AMSMATH, BEAMER, BIBTEX, CONTEXT, LATEX, TEX, \
  LESSON_TYPES, TEXT, MULT_CHOICE, TRUE_FALSE, FILL_IN, QUESTION_TYPES

QUIZ = 'quiz' # type of quiz question
LESSON = 'lesson' # type of lesson question
VIDEO_SERIES = 'video-series' # type of lesson question

import xml.etree.ElementTree as ET
QUESTION_FILE_NAME = "questions.xml"

# Global variables spare me from putting them in the call of each fcn.
VERBOSE = False
DEBUG = False

TRUE_STRINGS = ['true', 't']  # strings taken to be True (case-insensitive) 
FALSE_STRINGS = ['false', 'f']  # strings taken to be False (case-insensitive) 
def is_true_string(s):
    return s.lower() in TRUE_STRINGS 
def is_false_string(s):
    return not(is_true_string(s)) 

class PTException(Exception):
    pass


def warn(s):
    t = 'WARNING: '+s+"\n"
    sys.stderr.write(t)
    sys.stderr.flush()

def error(s):
    t = 'ERROR! '+s
    sys.stderr.write(t)
    sys.stderr.flush()
    sys.exit(10)



# ========= utilities ==========
def _allbut(t):
    """Get a string giving the xml below the current node, not including that
    node.  Need it to get HTML.
      t  node
    """
    if t.text:
        s = [bytearray(t.text,'utf-8')]+[ET.tostring(child) for child in t]
    else:
        s = [ET.tostring(child) for child in t]
    r = b''.join(s)
    return r.decode('utf-8')


# ========= read file ==========
def read_question_file(fn=QUESTION_FILE_NAME):
    try:
        t = ET.parse(fn)
    except Exception as e:
        error("Cannot read the XML file {0:s}: {1:s}".format(fn, str(e)))
    return t


# ========= multiple choice ==========
def _gather_question_multiple_choice(t):
    r = {}
    r['type'] = MULT_CHOICE
    for a in ['subject', 'lesson']:
        r[a] = t.get(a)
    r['text'] = _allbut(t.find('text')) 
    answers = []
    any_true_yet = False
    for c in t.findall('multiple-choice-answer'):
        try:
            answer_value = c.get('value')
        except:
            answer_value = 'False'
        if answer_value.lower() in TRUE_STRINGS:
            if VERBOSE and any_true_yet:
                warn('Multiple Choice question with multiple correct values: {0:s}'.format(r['text']))
            any_true_yet = True
        answer_text = _allbut(c.find('text'))
        answer_comment_node = c.find('comment')
        if answer_comment_node is None:
            answer_comment = ''
        else:
            answer_comment = answer_comment_node.text
        answers.append({'value':answer_value, 
                        'text':answer_text, 
                        'comment': answer_comment})
    r['answers'] = answers
    if VERBOSE and not(any_true_yet):
        warn('Multiple choice question with no correct values: {0:s}'.format(r['text']))
    return r

def _print_question_multiple_choice(d):
    r = ["Multiple choice: "]
    r.append("\n  Subject: {0:s}; Lesson {1:s}".format(d['subject'], d['lesson']))
    r.append("\n  Text: {0:s}".format(d['text']))
    r.append("\n  Answers")
    for x in d['answers']:
        r.append("\n    Value: {0:s};  Text: {1:s};  Comment: {2:s}".format(x['value'], x['text'], x['comment']))
    print("".join(r))

def handle_question_multiple_choice(t):
    d = _gather_question_multiple_choice(t)
    # _print_question_multiple_choice(d)
    return d

# ========= True/False ==========
def _gather_question_true_false(t):
    r = {}
    r['type'] = TRUE_FALSE
    for a in ['subject', 'lesson']:
        r[a] = t.get(a)
    r['text'] = _allbut(t.find('text'))
    any_true_yet = False
    answers = []
    for c in t.findall('true-false-answer'):
        answer_id = c.get('id')  # is this the one labelled "T" or labelled "F"?
        answer_value = c.get('value') # is this one right?
        try:
            answer_value = c.get('value')
        except:
            answer_value = 'False'
        if answer_value.lower() in TRUE_STRINGS:
            if VERBOSE and any_true_yet:
                warn('True/False question with multiple correct values: {0:s}'.format(r['text']))
            any_true_yet = True
        answer_comment_node = c.find('comment')
        if answer_comment_node is None:
            answer_comment = ''
        else:
            answer_comment = answer_comment_node.text
        answers.append({'id':answer_id, 
                        'value':answer_value, 
                        'comment': answer_comment})
    r['answers'] = answers
    if VERBOSE and not(any_true_yet):
        warn('True/False question with no correct values: {0:s}'.format(r['text']))
    return r

def _print_question_true_false(d):
    r = ["True/False: "]
    r.append("\n  Subject: {0:s}; Lesson: {1:s}".format(d['subject'], d['lesson']))
    r.append("\n  Text: {0:s}".format(d['text']))
    r.append("\n  Answers")
    for x in d['answers']:
        r.append("\n    Value: {0:s};  Id: {1:s};  Comment: {2:s}".format(x['value'], x['id'], x['comment']))
    print("".join(r))

def handle_question_true_false(t):
    d = _gather_question_true_false(t)
    # _print_question_true_false(d)
    return d

# ========= Text question ==========
def _gather_question_text(t):
    r = {}
    r['type'] = TEXT
    for a in ['subject', 'lesson']:
        r[a] = t.get(a)
    r['text'] = _allbut(t.find('text'))
    # try:
    #     r['runlatex'] = t.get('runlatex')
    # except:
    #     r['runlatex'] = 'False'    
    # if VERBOSE and not(r['runlatex'].lower() in TRUE_STRINGS+FALSE_STRINGS):
    #     warn("Text question with confused 'runlatex' attribute: {0:s}".format(r['text']))
    any_terms_yet = False
    answers = []
    for c in t.findall('text-answer'):
        try:
            answer_text = _allbut(c)
        except:
            answer_text = ''
        answer_search_term = c.get('search-term')
        answers.append({'search-term': answer_search_term, 'text':answer_text})
        any_terms_yet = True
    r['answers'] = answers
    if VERBOSE and not(any_terms_yet):
        warn('Text question with no search terms: {0:s}'.format(r['text']))
    return r

def _print_question_text(d):
    r = ["Text Question: "]
    # r.append("\n  Subject: {0:s}; Lesson: {1:s}; Runlatex: {2:s}".format(d['subject'], d['lesson'], d['runlatex']))
    r.append("\n  Subject: {0:s}; Lesson: {1:s}".format(d['subject'], d['lesson']))
    r.append("\n  Text: {0:s}".format(d['text']))
    r.append("\n  Answers")
    for x in d['answers']:
        r.append("\n    Search term: {0:s}".format(x['search-term']))
        r.append("\n    Text: {0:s}".format(x['text']))
    print("".join(r))

def handle_question_text(t):
    d = _gather_question_text(t)
    # _print_question_text(d)
    return d
    

# ========= Fill-in question ==========
def _gather_question_fill_in(t):
    r = {}
    r['type'] = FILL_IN
    for a in ['subject', 'lesson']:
        r[a] = t.get(a)
    r['text'] = _allbut(t.find('text'))
    any_terms_yet = False
    answers = []  # expect it to be of length one
    for c in t.findall('fill-in-answer'):
        answer_answer = c.get('answer')
        try:
            comment = t.find('comment').text
        except:
            comment = ''
        answers.append({'answer': answer_answer, 'comment': comment})
        any_terms_yet = True
    r['answers'] = answers
    if VERBOSE and not(any_terms_yet):
        warn('Fill-in question with no answers: {0:s}'.format(r['text']))
    if VERBOSE and (len(answers) > 1):
        warn('Fill-in question with multiple answers: {0:s}'.format(r['text']))
    return r

def _print_question_fill_in(d):
    r = ["Fill-in Question: "]
    r.append("\n  Subject: {0:s}; Lesson: {1:s}".format(d['subject'], d['lesson']))
    r.append("\n  Text: {0:s}".format(d['text']))
    r.append("\n  Answers")
    for x in d['answers']:
        r.append("\n    Answer: {0:s}; Comment: {1:s}".format(x['answer'], x['comment']))
    print("".join(r))

def handle_question_fill_in(t):
    d = _gather_question_fill_in(t)
    # _print_question_fill_in(d)
    return d



# ========= Quiz question ==========

def _gather_question_quiz(t):
    r = {}
    r['title'] = _allbut(t.find('title'))
    r['type'] = QUIZ
    for a in ['subject', 'lesson']:
        r[a] = t.get(a)
    try:
        r['lesson'] = int(r['lesson'])
    except:
        warn('Quiz question lesson not an integer (setting to 1): {0:s} for quiz {1:s}'.format(r['lesson'], r['title']))
        r['lesson'] = 1
    for k in ['intro', 'task']:
        n = t.find(k)
        if not(n is None):
            r[k] = _allbut(t.find(k))
        else:
            r[k] = ''
    r['search-terms'] = []
    for c in t.findall('search-term'):
        try:
            search_term_value = c.get('value')
        except:
            warn('Quiz question {0:s} has a search term with no value attribute (ignoring): for quiz {1:s}'.format(r['lesson'], r['title']))
        else:
            r['search-terms'].append((search_term_value,_allbut(c)))
    for k in ['desired-output', 'body']:
        n = t.find(k)
        if not(n is None):
            r[k] = _allbut(n)
        else:
            r[k] = ''
    if not(r['body']) and not(r['desired-output']):
        warn('Quiz question has neither body nor desired output: for quiz {0:s}'.format(r['title']))
    return r

def _print_question_quiz(d):
    r = ["Quiz: "]
    r.append("\n  Subject: {0:s}; Question number: {1:s}".format(d['subject'], str(d['lesson'])))
    r.append("\n  Title: {0:s}".format(d['title']))
    r.append("\n  Intro: {0:s}".format(d['intro']))
    r.append("\n  Task: {0:s}".format(d['task']))
    r.append("\n  Search terms:")
    for (v,c) in d['search-terms']:
        r.append("\n    value: {0:s}  comment: {1:s}".format(v,c))
    if d['body'] is None:
        r.append("\n  No body")
    else:
        r.append("\n  Body: {0:s}".format(d['body']))
    if d['desired-output'] is None:
        r.append("\n  No desired-output")
    else:
        r.append("\n  Desired-output: {0:s}".format(d['desired-output']))
    print("".join(r))

def handle_question_quiz(t):
    d = _gather_question_quiz(t)
    # _print_question_quiz(d)
    return d



# ========= Lessons ==========
def _gather_lesson(t):
    r = {}
    r['title'] = _allbut(t.find('title'))
    r['type'] = LESSON
    for a in ['subject', 'lesson']:
        r[a] = t.get(a)
    try:
        r['lesson'] = int(r['lesson'])  # page number
    except:
        warn('Lesson question lesson number not an integer (setting to 1): {0:s} for quiz {1:s}'.format(r['lesson'], r['title']))
        r['lesson'] = 1
    for k in ['intro-text', 'body-text']:
        n = t.find(k)
        if (n is None):
            r[k] = '--no intro text found--'
        else:
            r[k] = _allbut(n)
    if not(r['body-text']) and not(r['intro-text']):
        warn('Lesson has neither body text nor intro text: for lesson {0:s}'.format(r['title']))
    # for k in ['video', 'image']:
    #     n = t.find(k)
    #     r[k] = n.get('fn')
    for n in t.findall('lesson-doc'):
        if not('lesson-doc' in r):
            r['lesson-doc'] = []
        r['lesson-doc'].append({'srcfn':n.get('srcfn'),
                                'imgfn':n.get('imgfn'),
                                'name':n.get('name')})
    # print("Lesson structure: ")
    # pprint.pprint(r)
    return r

def _print_lesson(d):
    r = ["Lesson: "]
    r.append("\n  Subject: {0:s}; Lesson number: {1:s}".format(d['subject'], str(d['lesson'])))
    r.append("\n  Title: {0:s}".format(d['title']))
    r.append("\n  Intro-text: {0:s}".format(d['intro-text']))
    r.append("\n  Body-text: {0:s}".format(d['body-text']))
    # r.append("\n  Files: video {0:s}, image {1:s}".format(d['video'], d['image']))
    if 'lesson-doc' in d:
        r.append("\n  Documents:")
        for dex,ld in enumerate(d['lesson-doc']):
            r.append("\n    src {0:s}, img {1:s}, name {2:s}".format(ld['srcfn'], ld['imgfn'], ld['name']))
    print("".join(r))

def handle_lesson(t):
    d = _gather_lesson(t)
    # _print_lesson(d)
    return d




# ========= Video series ==========
# Note that there may be multiple video series; this just handles one
def _gather_video_series(t):
    r = {}
    r['type'] = VIDEO_SERIES
    for a in ['subject', 'series']:
        r[a] = t.get(a,None)
        if r[a] is None:
            error('Video series element missing attribute {0:s}'.format(a))
    e = t.find('description')
    if e is None:
        error('Video series {0:s} for subject {1:s} has no description'.format(r['series'], r['subject']))
    r['description'] = e.text.strip()
    r['lessons'] = {}
    for lesson in t.findall('lesson'):
        lesson_no = lesson.get('lesson',None)
        if lesson_no is None:
            error('Video series lesson element missing lesson attribute: series {0:s} for subject {1:s}'.format(r['series'],r['subject']))
        try:
            lesson_no = int(lesson_no)
        except:
            error('Video series lesson element {0:d} lesson attribute is not an integer: series {1:s} for subject {2:s}'.format(lesson_no,r['series'],r['subject']))
        r['lessons'][lesson_no] = {}
        # get the video filename
        video = lesson.find('video')
        if video is None:
            error('Video series lesson {0:d} element missing video attribute: series {1:s} for subject {2:s}'.format(lesson_no,r['series'],r['subject']))
        video_fn = video.get('fn',None)
        if video_fn is None:
            error('Video series lesson {0:d} element missing video element fn attribute: series {1:s} for subject {2:s}'.format(lesson_no,r['series'],r['subject']))
        r['lessons'][lesson_no]['video'] = video_fn
        # optionally get the image fn
        r['lessons'][lesson_no]['image'] = None 
        image = lesson.find('image')
        if image is not None:
            image_fn = image.get('fn',None)
            r['lessons'][lesson_no]['image'] = image_fn 
    return r

def _print_video_series(d):
    r = ["Video series: "]
    r.append("\n  Subject: {0:s}; Series: {1:s}".format(d['subject'], str(d['series'])))
    r.append("\n  Description: {0:s}".format(d['description']))
    for lesson_no in r['lessons']:
        r.append("\n    Lesson number {0:d}".format(lesson_no))
        r.append("\n      video url {0:s}".format(r['lessons'][lesson_no]['video']))
        if 'image' in r['lessons'][lesson_no]:
            r.append("\n      image url {0:s}".format(str(r['lessons'][lesson_no]['image'])))
    print("".join(r))

def handle_video_series(t):
    d = _gather_video_series(t)
    # _print_question_quiz(d)
    return d





# ===================

def get_data(fn=QUESTION_FILE_NAME):
    """Read the data from the XML file, return a structure:
    {subject -> { lesson number -> {dict of question data}}}
      fn  name of question file
    """
    t = read_question_file(fn)
    q, r, s, v = {}, {}, {}, [] # quizzes, questions, lessons. Format: subject -> {lesson -> [question_info, ...]}
    for child in t.getroot():
        if child.tag == 'fill-in-question':
            d = handle_question_fill_in(child)
        elif child.tag == 'multiple-choice-question':
            d = handle_question_multiple_choice(child)
        elif child.tag == 'text-question':
            d = handle_question_text(child)
        elif child.tag == 'true-false-question':
            d = handle_question_true_false(child)
        elif child.tag == 'quiz-question':
            d = handle_question_quiz(child)
        elif child.tag == 'lesson':
            d = handle_lesson(child)
        elif child.tag == 'video-series':
            d = handle_video_series(child)
        else:
            warn('Unknown tag: {0:s}.  Skipping.'.format(child.tag))
            continue
        if not(d['subject'] in [AMSMATH, BEAMER, CONTEXT, LATEX, TEX]):
            warn('Unknown subject {0:s} for question: {1:s}.  Skipping.'.format(subject,r[subject]['text']))
            continue
        # Types of information: quizzes, lessons, questions, video series
        if d['type'] == QUIZ:
            if not(d['subject'] in q):
                q[d['subject']] = {}
            try:
                lesson_no = int(d['lesson']) # even quizzes are numbered as lessons
            except:
                warn('Quiz lesson number {0:s} is not an integer: {1:s}. Skipping.'.format(d['lesson'],d['text']))
                continue
            q[d['subject']][lesson_no]=d
        elif d['type'] == LESSON:
            if not(d['subject'] in s):
                s[d['subject']] = {}
            try:
                lesson_no = int(d['lesson']) 
            except:
                warn('Lesson number {0:s} is not an integer: {1:s}. Skipping.'.format(d['lesson'],d['text']))
                continue
            s[d['subject']][lesson_no]=d
        elif d['type'] == VIDEO_SERIES:
            v.append(d)
        else:
            if not(d['subject'] in r):
                r[d['subject']] = {}
            try:
                lesson_no = int(d['lesson'])
            except:
                warn('Lesson number {0:s} is not an integer: {1:s}. Skipping.'.formet(d['lesson'],d['text']))
                continue
            if not(lesson_no in r[d['subject']]):
                r[d['subject']][lesson_no] = []
            r[d['subject']][lesson_no].append(d)
    return q,r,s,v


#==================================================================
def main (args):
    if 'filename' in args:
        q,r,s,v = get_data(args['filename'])
    else:
        q,r,s,v = get_data()
    if args['lessons'] or DEBUG:
        print("============\n structure s for lessons:")
        pprint.pprint(s)
    if args['questions'] or DEBUG:
        print("============\n structure r for questions:")
        pprint.pprint(r)
    if args['quizzes'] or DEBUG:
        print("============\n structure q for quizzes:")
        pprint.pprint(q)
    if args['video'] or DEBUG:
        print("============\n structure v for video series:")
        pprint.pprint(v)

#==================================================================
if __name__ == '__main__':
    try:
        start_time = time.time()
        parser = argparse.ArgumentParser(description=globals()['__doc__'])
        parser.add_argument('--filename','-f', action='store', default=globals()['QUESTION_FILE_NAME'], help="Name of XML file containing information")
        parser.add_argument('--lessons','-l', action='store_true', default=False, help="Print the contents of the lessons structure")
        parser.add_argument('--questions','-q', action='store_true', default=False, help="Print the contents of the questions structure")
        parser.add_argument('--quizzes','-z', action='store_true', default=False, help="Print the contents of the quizzes structure")
        parser.add_argument('--video','-s', action='store_true', default=False, help="Print the contents of the video series structures")
        parser.add_argument('-v','--version', action='version', version='%(prog)s '+globals()['__version__'], help="Show the version number")
        parser.add_argument('-D', '--debug', action='store_true', default=False, help='run debugging code')
        parser.add_argument('-V', '--verbose', action='store_true', default=False, help='give verbose output')
        args = parser.parse_args()
        args = vars(args)
        if ('debug' in args) and args['debug']: 
            DEBUG = True
        if ('verbose' in args) and args['verbose']: 
            VERBOSE = True
        main(args)
    except KeyboardInterrupt: # Ctrl-C
        print("Keyboard interruption")
    except SystemExit: # sys.exit()
        print("System exit")
        traceback.print_exc()
    except Exception as e:
        print('UNEXPECTED OUTCOME')
        print(str(e))
        traceback.print_exc()
        os._exit(1)
 
    if VERBOSE: 
        print('elapsed secs: {0:2f}'.format(time.time()-start_time))
    sys.exit(0)
