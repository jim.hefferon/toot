#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test the program.
"""
__version__ = '0.5.0'
__author__ = 'Jim Hefferon'
__license__ = 'GPL 3'

import sys, os, os.path, re, pprint, argparse, traceback, time
import unittest
import run_latex

# Global variables spare me from putting them in the call of each fcn.
VERBOSE = False
DEBUG = False
run_latex.VERBOSE = VERBOSE
run_latex.DEBUG = DEBUG

class RLTException(Exception):
    pass

# Used for a quick look 
def save_pdf_file(s,fn='output'):
    """Save the input string as a PDF file.
    """
    if len(s) == 0:
        return False
    output_fn = fn+'.pdf'
    # remove the file, in case s is None so I don't think that the run went OK
    try:
        os.remove(output_fn)
    except OSError:
        print("==> save_pdf_file: NO PDF FILE")
    if not(s is None):
        f = open(fn+'.pdf', 'wb')
        f.write(s)
        f.close()
    return True

# Used for a quick look 
def save_log_file(s,fn='output'):
    """Save the input string as a PDF file.
    """
    output_fn = fn+'.log'
    # remove the file, in case s is None so I don't think that the run went OK
    try:
        os.remove(output_fn)
    except OSError:  # dne
        # print("==> save_log_file: NO LOG FILE")
        pass
    if s is None:
        return False
    else:
        f = open(output_fn, 'w')
        f.write(s)
        f.close()
    return True

# A file with multiple pages
BIGGER_FILE = r"""\documentclass{article}
\usepackage{lipsum}
\begin{document}
\lipsum[1]
\section{One}
\lipsum[2-3]
\subsection{One 1}
\lipsum[4-6]
\subsection{One 2}
\lipsum[7]
\subsection{One 3}
\lipsum[8]
\section{Two}
\subsection{Two 1}
\lipsum[9-10]
\end{document}
"""

# A file with a simple error
MISSPELLING_FILE = r"""\documentclass{article}
\usepackage{lipsum}
\begin{document}
\lipsum[1]
\end{documetn}  % error here
"""

# Run forever; from Michael Doob http://www.tug.org/TUGboat/tb31-2/tb98doob.pdf
RUNLONGTIME_FILE = r"""\documentclass{article}
\begin{document}
\newcounter{cnt}
\loop
\stepcounter{cnt}
\ifnum \value{cnt}<500000
\repeat
\end{document}
"""

BIG_FILE = r"""\documentclass{article}
\begin{document}
\newcounter{cnt}
\loop
\thecnt\newpage \stepcounter{cnt}
\ifnum \value{cnt}<10000
\repeat
\end{document}
"""

class TestRuns(unittest.TestCase):
    """See if the code runs at all
    """

    def test_simple(self):
        """Test the simplest functionality
        """
        rc,pdf,log = run_latex.run_latex(run_latex.DUMMY)
        self.assertTrue(rc == 0,"Expected rc=0 from the DUMMY test")
        # r = save_pdf_file(pdf)
        rc,pdf,log = run_latex.run_latex(BIGGER_FILE)
        r = save_pdf_file(pdf)
        self.assertTrue(r,"Failed to get a PDF out of the bigger test")

    def test_error(self):
        """Test LaTeX files with errors 
        """
        rc,pdf,log = run_latex.run_latex(MISSPELLING_FILE)
        self.assertTrue(rc != 0,"Expected return code from misspell file to be nonzero.")
        r = save_log_file(log)
        self.assertTrue(r,"Failed to get a log out of the misspelling test")
        k = run_latex.get_error(log)
        print(k)
        
    def test_resource_limits(self):
        """Test LaTeX files that hit resource limits 
        """
        rc,pdf,log = run_latex.run_latex(RUNLONGTIME_FILE) # rc = 137
        self.assertTrue(rc != 0,"Expected return code from long time running file to be nonzero.")
        print("rc = {0}".format(str(rc)))
        self.assertTrue(pdf is None,"Unexpectedly got a PDF file from the misspelling test")
        r = save_log_file(log)
        self.assertFalse(r,"Uexpectedly got a logfile out of the misspelling test")

        rc,pdf,log = run_latex.run_latex(BIG_FILE) # rc = 153
        self.assertTrue(rc != 0,"Expected return code from big file to be nonzero.")
        print("rc = {0}".format(str(rc)))
        self.assertTrue(pdf is None,"Unexpectedly got a PDF file from the big file test")
        r = save_log_file(log)
        self.assertFalse(r,"Unexpectedly got a logfile out of the big file test")
        




#==================================================================
def main (args):
    unittest.main()


#==================================================================
if __name__ == '__main__':
    unittest.main()
    # suite = unittest.TestLoader().loadTestsFromTestCase(TestRuns)
    # unittest.TextTestRunner().run(suite)

    # try:
    #     start_time = time.time()
    #     parser = argparse.ArgumentParser(description=globals()['__doc__'])
    #     parser.add_argument('-v','--version', action='version', version='%(prog)s '+globals()['__version__'])
    #     parser.add_argument('-D', '--debug', action='store_true', default=False, help='run debugging code')
    #     parser.add_argument('-V', '--verbose', action='store_true', default=False, help='verbose output')
    #     args = parser.parse_args()
    #     args = vars(args)
    #     if ('debug' in args) and args['debug']: 
    #         DEBUG = True
    #     if ('verbose' in args) and args['verbose']: 
    #         VERBOSE = True
    #     main(args)
    #     if VERBOSE: 
    #         print('elapsed: {0:2f} seconds'.format(time.time()-start_time))
    #     sys.exit(0)
    # except KeyboardInterrupt: # Ctrl-C
    #     print("Keyboard interruption")
    # except SystemExit: # sys.exit()
    #     print("System exit")
    #     traceback.print_exc()
    # except Exception as e:
    #     print('UNEXPECTED OUTCOME')
    #     print(str(e))
    #     traceback.print_exc()
    #     os._exit(1)
    # else:
    #     sys.exit(0)
