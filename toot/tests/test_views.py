#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Test the program.
"""
__version__ = '0.0.1'
__author__ = 'Jim Hefferon'
__license__ = 'GPL 3'

NOTES = """
1) if you get response.content, you have to decode it:
   response.content.decode("utf-8")
2) You have to go to the full page name, you cannot rely on the system to 
   correct the page, or else it does a GET and drops all the POST data.
3) Run things with something like this.
 python3 ./manage.py test tests.test_views.TestRegistration.test_change_password
"""

import sys, os, os.path, re, pprint, argparse, traceback, time
# import unittest
from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate, login, logout

from lessons.models_parameters import * # some global definitions used by multiple mods
from lessons.models import TootUser, TootUserCertificates

# Global variables spare me from putting them in the call of each fcn.
VERBOSE = False
DEBUG = False

from toot import views as toot_views
toot_views.VERBOSE, toot_views.DEBUG = DEBUG, VERBOSE

from bin import read_questions
realPath = os.path.realpath(__file__)  # /home/user/test/my_script.py
dirPath = os.path.dirname(realPath)  # /home/user/test
dirName = os.path.basename(dirPath) # test
QUESTIONS_PATH = os.path.realpath(os.path.join(dirPath,"../bin/questions.xml"))
# print("QUESTIONS_PATH="+QUESTIONS_PATH)
from lessons.management.commands import dblessoncontent


class LessonTestException(Exception):
    pass


def make_toot_user(username='cal', email='cal@smcvt.edu', password='12345678', realworld_name='calsmith'):
    u = User.objects.create_user(username,email=email,password=password) 
    tu = TootUser.objects.get(user=u)
    tu.realworld_name = realworld_name
    tu.save()
    return tu

def make_toot_user_certificate(tu,lesson_type=LATEX,certificate_id='a1'):
    tuc =  TootUserCertificates(toot_user = tu,
                                lesson_type=lesson_type,
                                certificate_id=certificate_id)
    tuc.save()
    return tuc

import lxml 
from lxml import etree
class html_page(object):
    """Encapsulates the parsing of an HTML page
    """
    def __init__(self, page_asstring):
        self.page_asstring = page_asstring
        self.root = self._parse()
        
    def _parse(self):
        return etree.HTML(self.page_asstring)
    
    def tostring(self):
        return etree.tostring(self.root, pretty_print=True).decode('utf-8')

    def get_formvalues(self):
        """Return dictionary of the input names and values.  Assumes a single
        form.
        """
        r = {}  
        form_node = self.root.find(".//form")
        for nd in form_node.findall(".//input"):
            if nd.get('type','').lower() == 'submit':
                r['submit'] = nd.get('value',None)
            else:
                r[nd.get('name')] = nd.get('value',None)
            # print("  name ="+nd.get('name','no name attribute')+" value="+nd.get('value','no value attribute'))
        return r

class TestWelcome(TestCase):
    """See if welcome.html works
    """

    def test_simple(self):
        """Test the simplest functionality
        """
        client = Client()
        response = client.get('/welcome/',follow=True)
        self.assertEqual(response.status_code,200,'welcome page not found')


class TestRegistration(TestCase):
    """See if registration and session management works
    """
    def setUp(self): 
        quizzes, questions, lessons, video_series_list = read_questions.get_data(fn=QUESTIONS_PATH) 
        dblessoncontent.store_lessons(lessons) 
        dblessoncontent.store_questions(questions) 
        dblessoncontent.store_video_series(video_series_list) 
        dblessoncontent.store_quizzes(quizzes) 

    def tearDown(self):
        dblessoncontent.wipe_video_series()
        dblessoncontent.wipe_quizzes()
        dblessoncontent.wipe_questions()
        dblessoncontent.wipe_lessons()

    def test_simple(self):
        """Test the simplest functionality
        """
        self.client = Client()
        # Establish a good new user
        uname, pswd = 'newuser', '12345678'
        data = {'username': uname,
                'password1':pswd,
                'password2':pswd,
                'email':'newuser@example.com'}
        response = self.client.post('/welcome/', data=data, follow=True, secure=False)
        # Did the user get logged in?
        self.assertEqual(response.status_code,200,'login page not found')
        # Did the user get sent to the home page?
        self.assertIn('home.html',[x.name for x in response.templates],'user was not sent to home page on login')
        # A session now exists?
        session = self.client.session
        self.assertIn('username',session,'session cookie does not have expected structure')
        # Now log out
        response = self.client.get('/accounts/logout', data=data, follow=True, secure=False)
        # print("response.templates {0:s}".format(pprint.pformat([x.name for x in response.templates])))
        self.assertIn('registration/logged_out.html',[x.name for x in response.templates],'user was not sent to logged_out page on get-ting the log out URL')

    def test_logout_and_login(self):
        """Test logging out and logging back in
        """
        self.client = Client()
        # Establish a good new user
        uname, pswd = 'newuser1', '12345678'
        data = {'username': uname,
                'password1':pswd,
                'password2':pswd,
                'email':'newuser@example.com'}
        response = self.client.post('/welcome/', data=data, follow=True, secure=False)
        # Verify that login worked, and session exists
        self.assertEqual(response.status_code,200,'login page not found')
        self.assertIn('home.html',[x.name for x in response.templates],'user was not sent to home page on login')
        session = self.client.session
        self.assertIn('username',session,'session cookie does not have expected structure')
        # Log out
        response = self.client.get('/accounts/logout', data=data, follow=True, secure=False)
        self.assertIn('registration/logged_out.html',[x.name for x in response.templates],'user was not sent to logged_out page on get-ting the log out URL')
        # Get to page to log back in
        log_back_data = {'username': uname,
                'password':pswd}
        response = self.client.get('/welcome/?login=True', data={}, follow=True, secure=False)
        self.assertEqual(response.status_code,200,'log in page not found')
        self.assertIn('welcome.html',[x.name for x in response.templates],'user was not sent to login page on trying to log back in')
        # print("response.content {0:s}".format(str(response.content.decode("utf-8"))))
        self.assertIn('Username',response.content.decode("utf-8"),'user was not given login form on asking to log back in')
        # Log back in
        response = self.client.post('/welcome/', data=log_back_data, follow=True, secure=False)
        # Verify that login worked, and session exists
        self.assertEqual(response.status_code,200,'login page not found')
        self.assertNotIn('errors in the input',response.content.decode("utf-8"),'Asking to log back in gives an error')
        # print("response.content {0:s}".format(str(response.content)))
        # print("response.templates {0:s}".format(pprint.pformat([x.name for x in response.templates])))
        self.assertIn('home.html',[x.name for x in response.templates],'user was not sent to home page on login')
        session = self.client.session
        self.assertIn('username',session,'session cookie does not have expected structure')

    def _get_realworld_name(self,c):
        rw_regex = re.compile('<input id="id_realworld_name"[^>]*?value="([^"]*)" />',re.UNICODE)
        m = rw_regex.search(c)
        if m:
            return m.group(1)
        else:
            return None
    def _get_email(self,c):
        rw_regex = re.compile('<input id="id_email"[^>]*?value="([^"]*)" />',re.UNICODE)
        m = rw_regex.search(c)
        if m:
            return m.group(1)
        else:
            return None

    def test_change_profile(self):
        """Test when user changes information
        """
        self.client = Client()
        # Establish a good new user
        uname, pswd, email = 'newuser1', '12345678', 'newuser@example.com'
        data = {'username': uname,
                'password1':pswd,
                'password2':pswd,
                'email':email}
        response = self.client.post('/welcome/', data=data, follow=True, secure=False)
        # print("response.request {0:s}".format(pprint.pformat(response.request)))
        # Verify that login worked, and session exists
        self.assertEqual(response.status_code,200,'login page not found')
        self.assertIn('home.html',[x.name for x in response.templates],'user was not sent to home page on login')
        session = self.client.session
        self.assertIn('username',session,'session cookie does not have expected structure')
        # Go to profile page
        response = self.client.get('/lessons/profile', data={}, follow=True, secure=False)
        # print("response.templates {0:s}".format(pprint.pformat([x.name for x in response.templates])))
        self.assertIn('registration/profile_change.html',[x.name for x in response.templates],'user was not sent to profile change page')
        # Check email listed
        response = self.client.get('/lessons/profile/', follow=True, secure=False)
        self.assertEqual(email,self._get_email(response.content.decode("utf-8")),'email listed on profile page is not expected one: '+email)
        # Change email
        new_email = 'joe@fred.com'
        response = self.client.post('/lessons/profile/', data={"email":new_email}, follow=False, secure=False)
        response = self.client.get('/lessons/profile/', follow=True, secure=False)
        self.assertEqual(new_email,self._get_email(response.content.decode("utf-8")),'email listed on profile page is not expected one: '+new_email)
        # Check real world name is not listed yet
        response = self.client.get('/lessons/profile', data={}, follow=True, secure=False)
        self.assertIsNone(self._get_realworld_name(response.content.decode("utf-8")),'found real world name but it is not set yet')
        # Set real world name 
        realworld_name = 'archibald conner'
        response = self.client.post('/lessons/profile/', data={'realworld_name':realworld_name}, follow=True, secure=False)
        self.assertEqual(response.status_code,200,'realworld_name set post not accepted')
        response = self.client.get('/lessons/profile/', data={}, follow=True, secure=False)
        self.assertEqual(realworld_name,self._get_realworld_name(response.content.decode("utf-8")),'realworld_name not set')
        new_realworld_name = 'a bald conner'
        response = self.client.post('/lessons/profile/', data={'realworld_name':new_realworld_name}, follow=True, secure=False)
        self.assertEqual(response.status_code,200,'realworld_name change post not accepted')
        response = self.client.get('/lessons/profile/', data={}, follow=True, secure=False)
        self.assertEqual(new_realworld_name,self._get_realworld_name(response.content.decode("utf-8")),'realworld_name not changed')


    def test_change_password(self):
        """Test when user changes the password
        """
        self.client = Client()
        # Establish a good new user
        uname, pswd, email = 'newuser1', '12345678', 'newuser@example.com'
        data = {'username': uname,
                'password1':pswd,
                'password2':pswd,
                'email':email}
        response = self.client.post('/welcome/', data=data, follow=True, secure=False)
        # print("response.request {0:s}".format(pprint.pformat(response.request)))
        # Verify that login worked, and session exists
        self.assertEqual(response.status_code,200,'login page not found')
        self.assertIn('home.html',[x.name for x in response.templates],'user was not sent to password change done page on completing a password ')
        session = self.client.session
        self.assertIn('username',session,'session cookie does not have expected structure')
        self.assertEqual(uname,session['username'],'session cookie unexpected name')
        # Change password
        new_pswd = 'abcdefgh'
        data = {'old_password': pswd,
                'new_password1':new_pswd,
                'new_password2':new_pswd}
        response = self.client.post('/accounts/password_change/', data=data, follow=True, secure=False)
        self.assertEqual(response.status_code,200,'password change not accepted')
        
        self.assertIn('registration/password_change_done.html',[x.name for x in response.templates],'user was not sent to password_change_done.html page on password change')
        # Log out
        response = self.client.get('/accounts/logout', data={}, follow=True, secure=False)
        self.assertEqual(response.status_code,200,'logout not accepted after password change')
        self.assertIn('registration/logged_out.html',[x.name for x in response.templates],'user was not sent to logged_out page on logging out after password change')
        # Get to page to log back in
        log_back_data = {'username': uname,
                'password':new_pswd}
        response = self.client.post('/welcome/', data=log_back_data, follow=True, secure=False)
        self.assertEqual(response.status_code,200,'log in not accepted with new password after password change')
        self.assertIn('home.html',[x.name for x in response.templates],'user was not sent to login page on logging back inafter password change')


    def test_change_profile(self):
        """Test when user changes the profile
        """
        self.client = Client()
        # Establish a good new user
        uname, pswd, email = 'newuser1', '12345678', 'newuser@example.com'
        data = {'username': uname,
                'password1':pswd,
                'password2':pswd,
                'email':email}
        response = self.client.post('/welcome/', data=data, follow=True, secure=False)
        # print("response.request {0:s}".format(pprint.pformat(response.request)))
        # Verify that login worked, and session exists
        self.assertEqual(response.status_code,200,'login page not found')
        self.assertIn('home.html',[x.name for x in response.templates],'user was not sent to password change done page on completing a password ')
        session = self.client.session
        self.assertIn('username',session,'session cookie does not have expected structure')
        self.assertEqual(uname,session['username'],'session cookie unexpected name')
        # Ask for a change of profile
        # new_pswd = 'abcdefgh'
        # data = {'old_password': pswd,
        #         'new_password1':new_pswd,
        #         'new_password2':new_pswd}
        response = self.client.get('/lessons/profile/', follow=True, secure=False)
        page = html_page(response.content.decode('utf-8'))
        # print("tostring="+page.tostring())
        # print("get_formvalues="+pprint.pformat(page.get_formvalues()))
        formvalues = page.get_formvalues()
        self.assertEqual(response.status_code,200,'not sent to profile change page')      
        self.assertIn('registration/profile_change.html',[x.name for x in response.templates],'user was not sent to profile_change.html page on request of that page')
        # Change the email
        new_email = 'email1@smcvt.edu'
        data = formvalues
        data['email'] = new_email
        response = self.client.post('/lessons/profile/', data=data, follow=True, secure=False)
        print("response page="+response.content.decode('utf-8'))
        self.assertEqual(response.status_code,200,'profile change email does not return status 200')      
        self.assertIn('registration/home.html',[x.name for x in response.templates],'user was not sent to home.html page on changing profile email')
        # # Log out
        # response = self.client.get('/accounts/logout', data={}, follow=True, secure=False)
        # self.assertEqual(response.status_code,200,'logout not accepted after password change')
        # self.assertIn('registration/logged_out.html',[x.name for x in response.templates],'user was not sent to logged_out page on logging out after password change')
        # # Get to page to log back in
        # log_back_data = {'username': uname,
        #         'password':new_pswd}
        # response = self.client.post('/welcome/', data=log_back_data, follow=True, secure=False)
        # self.assertEqual(response.status_code,200,'log in not accepted with new password after password change')
        # self.assertIn('home.html',[x.name for x in response.templates],'user was not sent to login page on logging back inafter password change')




# class TestCertificate(TestCase):
#     """Certificate granting
#     """
#     def setUp(self):
#         self.tu = make_toot_user()
#         if VERBOSE:
#             print("test_views.py TestCertificate.setUp(): user name {0:s}".format(self.tu.user.username))
#         self.tuc = make_toot_user_certificate(self.tu)
#         if VERBOSE:
#             print("test_views.py  TestCertificate.setUp(): certificate_id {0:s}".format(self.tuc.certificate_id))

#     def test_simple(self):
#         """Test that it works
#         """
#         client = Client()
#         response = client.get('/lessons/certificate/latex',follow=True)
#         self.assertEqual(response.status_code,200,'Bad response for certificate')
#         pass


#==================================================================
def main (args):
    unittest.main()
    # suite = unittest.TestLoader().loadTestsFromTestCase(TestRuns)
    # unittest.TextTestRunner().run(suite)


#==================================================================
if __name__ == '__main__':
    try:
        start_time = time.time()
        parser = argparse.ArgumentParser(description=globals()['__doc__'])
        parser.add_argument('-v','--version', action='version', version='%(prog)s '+globals()['__version__'])
        parser.add_argument('-D', '--debug', action='store_true', default=False, help='run debugging code')
        parser.add_argument('-V', '--verbose', action='store_true', default=False, help='verbose output')
        args = parser.parse_args()
        args = vars(args)
        if ('debug' in args) and args['debug']: 
            DEBUG = True
        if ('verbose' in args) and args['verbose']: 
            VERBOSE = True
        main(args)
        if VERBOSE: 
            print('elapsed secs: {0:3.2f}'.format(time.time()-start_time))
        sys.exit(0)
    except KeyboardInterrupt as e: # Ctrl-C
        raise e
    except SystemExit as e: # sys.exit()
        raise e
    except Exception as e:
        print('UNEXPECTED OUTCOME: error is {0:s}'.format(str(e)))
        traceback.print_exc()
        os._exit(1)
