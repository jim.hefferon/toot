#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Test the program.
"""
__version__ = '0.0.1'
__author__ = 'Jim Hefferon'
__license__ = 'GPL 3'

import sys, os, os.path, re, pprint, argparse, traceback, time
# import unittest
from django.test import TestCase, Client

# Global variables spare me from putting them in the call of each fcn.
VERBOSE = False
DEBUG = False

class PTTException(Exception):
    pass


class TestRuns(TestCase):
    """See if the code runs at all
    """

    def test_simple(self):
        """Test the simplest functionality
        """



#==================================================================
def main (args):
    # unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(TestRuns)
    unittest.TextTestRunner().run(suite)


#==================================================================
if __name__ == '__main__':
    try:
        start_time = time.time()
        parser = argparse.ArgumentParser(description=globals()['__doc__'])
        parser.add_argument('-v','--version', action='version', version='%(prog)s '+globals()['__version__'])
        parser.add_argument('-D', '--debug', action='store_true', default=False, help='run debugging code')
        parser.add_argument('-V', '--verbose', action='store_true', default=False, help='verbose output')
        args = parser.parse_args()
        args = vars(args)
        if ('debug' in args) and args['debug']: 
            DEBUG = True
        if ('verbose' in args) and args['verbose']: 
            VERBOSE = True
        main(args)
        if VERBOSE: 
            print('elapsed secs: {0:3.2f}'.format(time.time()-start_time))
        sys.exit(0)
    except KeyboardInterrupt as e: # Ctrl-C
        raise e
    except SystemExit as e: # sys.exit()
        raise e
    except Exception as e:
        print('UNEXPECTED OUTCOME: error is {0:s}'.format(str(e)))
        traceback.print_exc()
        os._exit(1)
