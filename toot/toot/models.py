from django.db import models


class Lesson(models.Model):
    """Each page is a lesson
    """
    page_no = models.IntegerField(default=0) 
    video_fn = models.CharField(max_length=100)
    transcript_fn = models.CharField(max_length=100)
    title = models.CharField(max_length=200) 
    intro_text = models.TextField() 
    body_text = models.TextField()


FILL_IN_MAX_LENGTH = 100
class Question(models.Model):
    """Each question has an associated lesson (page), and is one of a small
    number of question types.
    """
    TEXT = 'TE'
    MULT_CHOICE = 'MC'
    TRUE_FALSE = 'TF'
    FILL_IN = 'FI'
    Q_TYPE_CHOICES = (
        (TEXT, 'Text'),
        (MULT_CHOICE, 'Multiple choice'),
        (TRUE_FALSE, 'True/False'),
        (FILL_IN, 'Fill in the blank'),
    )
    page_no = models.ForeignKey(TootPage, on_delete=models.CASCADE) # p it is on
    question_type = models.CharField(
        max_length=2,
        choices=Q_TYPE_CHOICES,
        default=TEXT,
    )    
    question_text = models.CharField(max_length=400) # Body of q
    run_tex = models.BooleanField(default = False) # Run LaTeX to test?
    text_response = models.TextField(max_length = 2000) 
    mult_choice = models.CharField(default = False) 
    true_false = models.BooleanField(default = False) 
    fill_in = models.CharField(max_length=FILL_IN_MAX_LENGTH) # Fill in blank


class SearchTerms(models.Model):
    """Terms to search for in text answers of questions
    """
    question = models.ForeignKey(Question, on_delete=models.CASCADE) # 
    term = models.CharField(max_length=100) 
    comment = models.CharField(max_length=1000) # Comment if term missing 


class MultChoice(models.Model):
    """Multiple choice questions need key-value list of options, and an 
    optional comment if that choice is made. 
    """
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    number_choices = models.SmallIntegerField(default=2)  # how many options?
    key_one = models.CharField(max_length=20, default="1")
    text_one = models.CharField(max_length=200, default="")
    comment_one = models.CharField(max_length=1000, default="")
    key_two = models.CharField(max_length=20, default="1")
    text_two = models.CharField(max_length=200, default="")
    comment_two = models.CharField(max_length=1000, default="")
    key_three = models.CharField(max_length=20, default="1")
    text_three = models.CharField(max_length=200, default="")
    comment_three = models.CharField(max_length=1000, default="")
    key_four = models.CharField(max_length=20, default="1")
    text_four = models.CharField(max_length=200, default="")
    comment_four = models.CharField(max_length=1000, default="")
    key_five = models.CharField(max_length=20, default="1")
    text_five = models.CharField(max_length=200, default="")
    comment_five = models.CharField(max_length=1000, default="")


class TF(models.Model):
    """Answer to a True/False question
    """
    question = models.ForeignKey(Question, on_delete=models.CASCADE) # 
    answer = models.BooleanField(default=False) 
    comment = models.CharField(max_length=1000) # Comment if answer wrong


class FillIn(models.Model):
    """Answer to a fill-in question
    """
    question = models.ForeignKey(Question, on_delete=models.CASCADE) # 
    answer = models.CharField(max_length=FILL_IN_MAX_LENGTH) 
    comment = models.CharField(max_length=1000) # Comment if answer wrong
