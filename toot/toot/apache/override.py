# override.py
# From: https://www.sitepoint.com/deploying-a-django-app-with-mod_wsgi-on-ubuntu-14-04/
# The override.py imports all settings and overrides any settings for
# production. For instance, databases and debug settings for production
# might be different from those of development, and you may want to
# separate them from the source code.

from toot.settings import *

DEBUG = True
# ALLOWED_HOSTS = ['localhost', 'joshua.smcvt.edu', 'smcvt.edu']
ALLOWED_HOSTS = ['*']
