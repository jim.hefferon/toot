\documentclass{book}
\renewcommand{\chaptername}{Video}

\usepackage{amsmath}
\usepackage{amsthm}
\newtheorem{thm}{Theorem}
\newtheorem{dfn}{Definition}

\usepackage{graphicx}
\usepackage[dvipsnames]{xcolor}

\usepackage{tikz}
\usetikzlibrary{arrows,automata,intersections,shapes,positioning}
\tikzset{%
  highlight/.style={rectangle,rounded corners,fill=red!15,draw,fill opacity=0.5,thick,inner sep=0pt}
}
\newcommand{\tikzmark}[2]{\tikz[overlay,remember picture,baseline=(#1.base)] \node (#1) {#2};}
%
\newcommand{\Highlight}[1][submatrix]{%
    \tikz[overlay,remember picture]{
    \node[highlight,fit=(left.north west) (right.south east)] (#1) {};}
}

\usepackage[left=1.25in,right=1.25in,top=1in,bottom=1in]{geometry}

\setlength{\parindent}{1.5em}

\usepackage{listings}
\lstset{ %
  basicstyle=\scriptsize\ttfamily,  % the size of the fonts 
  breaklines=false,                 % It sometimes ignores this. What the fuck?
  columns=fixed,
  % language=TeX,
  % linewidth=\textwidth,
  showspaces=false,               % show spaces adding particular underscores
  showstringspaces=false,         % underline spaces within strings
  showtabs=false,                 % show tabs within strings
  % xleftmargin=0.5\textwidth,
}

\usepackage{url}

% \usepackage{fvrb-ex}
% \fvset{gobble=0,xrightmargin=0.5\textwidth,fontsize=\scriptsize}

% \usepackage[export]{adjustbox}% http://ctan.org/pkg/adjustbox
% Show a document for a lesson, alongside source
\newcommand{\lessondoc}[1]{%
\begin{center}\hspace*{-15em}
  \begin{minipage}{0.6\textwidth}% 
   \fbox{\includegraphics[width=\textwidth]{#1.pdf}}%
  \end{minipage}\qquad%
  \begin{minipage}{0.4\textwidth}%
    \lstinputlisting[linewidth=\textwidth]{#1.tex}%
  \end{minipage}
\end{center}}


\usepackage{enumitem}
\setlist[description]{leftmargin=0em,
  labelindent=0em,
  listparindent=1.5\parindent,
  %before=\setlength{\listparindent}{-\leftmargin}
  }

\newenvironment{topic}{%
  \begin{description}
}{%
  \end{description}
}

\newcommand{\texworks}{\TeX works}
\newcommand{\tikzname}{Ti\textit{k}Z}

\usepackage{hyperref}
\hypersetup{
    pdftitle={LaTeX Tutorial},   
    pdfauthor={F McGuillicuddy (TUG)},  
    pdfsubject={LaTeX, TeX},   % subjects of the document
    colorlinks=true,      % false: boxed links; true: colored links
    linkcolor=black,      % color of internal links
    citecolor=black,      % color of links to bibliography
    filecolor=blue,       % color of file links
    urlcolor=blue         % color of external links
}

\title{Script for lessons}
\author{Jim Hef{}feron}
\date{2016-Sep-11}

\begin{document}
\maketitle
\frontmatter
\chapter{Preface}
This is a script of the tutorial videos.
A couple of notes.
\begin{enumerate}
\item This is a draft.  
  It no doubt contains errors.
  Thank you for corrections and comments.

\item This is an introduction for undergraduates.
  It is not intended for people who need more advanced usage, such
  as PhD thesis writers.
  That explains why there is little about writing 
  personal macros.
  It also explains why there is little about bibliographies
  or document classes other than article.

\item I'm illustrating by using emacs with \TeX Live.  
  We would have other people use basically the same script to 
  make videos of other environments.

\item I intend that on the screen is a shot of input paired with output.
  The \texttt{.tex} input docs used are downloadable from the lesson page, 
  so the user is sure that they have not mistyped.

  In some cases in this doc I show output next to the input.
  This is just a convenience for reading this doc.
  In some places that ouput doesn't work perfectly.
  In particular, the beamer doc is multipage but the output shown here is
  single page.
\end{enumerate}


\tableofcontents
\mainmatter

\chapter{Your first document}
Hello, I'm Fred McGuillicuddy.
Welcome to this introduction to \LaTeX{}.
This series of eleven videos takes you from beginner to
being able to produce almost any document an undergraduate would need. 

These videos are part of a sequence \url{http://joshua.smcvt.edu/lessons}.
Besides the videos, each lesson has some questions so you can 
check your understanding. 
And, at the end there is a quiz so you can get a certification.
It's free, so check it out.
Of course, watching the videos without taking the course works also.

\begin{topic}
\item[Introduction]
    \LaTeX{} is powerful software for making documents.
    It is well-known for its first-quality results, 
    particularly with mathematical material.
    Using \LaTeX{} is standard professional practice in mathematics, 
    physics, and computer science, and it is often used
    in other fields.
    It is suitable for thousand page books
    or five page lab reports, 
    for a teacher's projector presentation slides, and 
    for a student's homework hand-in. 

    \LaTeX{} is part of 
    a software family based around the typesetting engine \TeX{}. 
    These programs were created by scientists, for scientists.
    Naturally they are Free software. 

    Like all sophisticated programs, \LaTeX{} must be learned.
    That's why we offer these lessons.


\item[The software]
To write your own documents, for instance to follow along with
these lessons, you will need to use the software.
The way to learn how to use tools is to use them. 
You can either install the software on your computer 
or use it an at online site.

Installing on your computer is free and easy.
Visit \url{https://www.tug.org/texlive/} and follow the directions.
Online sites are also free to start (they may charge for advanced access).
Do a web search for ``use LaTeX online.''


\item[The videos]
You have lots of freedom on where an how you run \LaTeX.
Personally, I use a laptop with Ubuntu Linux.
I like things basic so I edit with a text editor,
I run \LaTeX{} from the command line, and I view the output in
a PDF viewer.

We have different versions of these videos so you can
look for one that matches what you have, whether you use a personal 
computer or an online site.
Or you can watch this series even though you have a different setup;
your screen may look a bit different but \LaTeX{} is exactly the same.


\item[Starting] 
I use the editor \textit{emacs}.
Normally to start a new \LaTeX{} document I create a subdirectory, and
create a file.
Name the file so it ends \texttt{.tex}.
We say this is the document's source file.

I have already put the document there, so I will open that
(you can get a copy of this file from the lesson page and avoid typos).
\lessondoc{lesson1/lesson1doc1}

You see that \LaTeX{} documents are a mix of text and commands.
The commands start with backslash. 

Every \LaTeX{} document has two parts, a preamble and a body.
On top is the preamble. 
For instance, this one declares that the document is an article.
Below the preamble,
starting with \lstinline!\begin{document}!
is the document body.

To turn this document source into PDF output,
save the file and then
run it through the \LaTeX{} program.
If you have programming experience then you will recognize making
\LaTeX{} documents has the same 
basic outline.  
We say that we use \LaTeX{} to  ``compile'' the 
source to the output. 

See the output with a PDF viewer.
Looks great!




\item[Mistakes happen]
You probably want to try it yourself.
That leaves the possibility that there is some hiccup, and so 
you need to know how to handle those.
 
Change the document so the last line has a misspelling.
\lstinputlisting{lesson1/lesson1doc2.tex}
\LaTeX{} give you an error message,
saying it is on line eight.
\begin{lstlisting}
! LaTeX Error: \begin{document} ended by \end{documetn}.

See the LaTeX manual or LaTeX Companion for explanation.
Type  H <return>  for immediate help.
 ...                                              
                                                  
l.8 \end{documetn}
                  
?
\end{lstlisting}
If you are working from a command line, 
to get out of the question mark prompt, type `x' and enter.
Sometimes also Control-d gets you out of an error.

Correct the error, save the file, and rerun.

Error messages can be frustrating.
One strategy is to 
rerun \LaTeX{} every paragraph or so.
If you get an error then you know pretty well where it is.
Another good strategy is to do a web search on the error message.

\item[Next]
The next lesson says more about the structure of a document.
See you then!
\end{topic}







% =================
\chapter{\LaTeX{} basics}
Hi, I'm Fred McGuillicuddy and
this is the second video of our series introducing you to \LaTeX{},

These videos are from the
course \url{https://joshua.smcvt.edu/lessons}.
The course is free; it adds some questions to each video to help you check
your understanding, and at the end will give you a certification.
Watching the videos without taking the course is fine, too.

\begin{topic}
\item[Commands]
Here is another look at your first document.
\lessondoc{lesson1/lesson1doc1}
\LaTeX{} documents are a mix of commands and text.
Commands begin with a backslash, \textbackslash.
For instance, the command \lstinline!\pi! 
produces the greek letter \( \pi \).

Some commands need inputs.
In \TeX{} these are called arguments;
one example is that 
\lstinline!\documentclass! has the argument \lstinline!article!.
These go between curly braces.
Some commands take more than one argument.

Some commands have arguments that are optional.
These go between square brackets.
With \lstinline!\usepackage[utf8]{inputenc}!, 
the argument \texttt{inputenc} is required but the \texttt{utf8} is optional.
(In this introduction we won't see much of optional arguments.) 

The \lstinline!\begin{document}! and \lstinline!\end{document}! are commands, 
but think of them as a matched pair.
We say that material between the two is in an \textit{environment}.
For these two, it is a \lstinline!document! environment.

Notice that the second and third line say \lstinline!\usepackage...!.
These commands bring in extra functionality.
(The first allows us to use more than the basic characters, for instance
to copy text from a web page, and the second gives us powerful math
capabilities.)
We will say more in a later lesson but for the moment just
know that we will always include these two lines.

A final point: 
we sometimes want to add a comment, marked by the percent sign. 
The part of the line after the percent is ignored by \LaTeX{}
but is there to help people reading the source file.
\begin{lstlisting}
Our data is not significant.  % TODO: check for outliers
\end{lstlisting}


\item[Separating content from format]
\LaTeX{} is not like a word processor. 
Users of word processors typically format the text while typing--- 
maybe as they write they click and drag to add spacing, or boldface, 
or centering.

\LaTeX{} instead nudges you to keep content separate from presentation.
You want to focus on what you are saying 
and leave putting it into place to the computer. 

The next document illustrates.
\LaTeX{} certainly has commands to explicitly paint the text, for instance 
the commands to make boldface and italics,
and the \lstinline!center! environment.
\lessondoc{lesson2/lesson2doc1}
But this document also shows examples of just declaring what
something is and letting \LaTeX{} place it.

The simplest example is paragraphs.
You don't make any start-of-paragraph indent, you just put in a blank line.
(If you use two or three blank lines instead of one, 
\LaTeX{} still just makes one paragraph.)
And although you may not notice, \LaTeX{} tries to avoid having only 
one line of a paragraph on a page, which can be awkward.

Another example is that 
you declare the \lstinline!\title{..}! and \lstinline!\author{..}!,
and later say \lstinline!\maketitle!.
You don't by-eye do font or vertical spacing; \LaTeX{} does that.
Computers are good at details.

The \lstinline!footnote{}! command keeps 
the text of the footnote appears near the text being
footnoted, rather than format it on the author's screen, so that
is also content-driven rather than presentation-driven.



\item[Special characters]   
We've seen that
\LaTeX{} uses the backslash to mark the start of a commands.
So how do you output a backslash?

Similarly, we've used dollar signs to mark mathematics,
curly braces as part of commands, 
and a percent sign for comments.
This shows how you get those characters as output.
\lessondoc{lesson2/lesson2doc2}
It also shows quotation marks.
To type open quotes, use two backticks.
To get close quotes, use two apostrophe's.



\item[Next]
The next lesson shows how to write mathematical material.
Enjoy!
\end{topic}







% =================
\chapter{Mathematics}
I'm Fred McGuillicuddy.
Welcome to the third in our introduction to \LaTeX{} series.
These videos are from
the course \url{http://joshua.smcvt.edu/lessons}.
Watching them without taking the course is fine also, of course.

If you are not taking the course, just watching the videos, and are 
interested in \LaTeX{} for things other than mathematics, then you could skip 
ahead to video six.


\begin{topic}
\item[Handling math]
Look at this document's first paragraph.
Compare the plain text to the mathematics text.
The mathematics has variables in italics, 
and the spacing is different, for instance around the equal sign. 
We must treat mathematical text specially.
\lessondoc{lesson3/lesson3doc1}
We do this
by putting the mathematical source text between a backslash and an open paren,
and a backslash and a closed paren.
Anytime you have mathematical phrase, even if it is just 
a single letter such as the variable \( x \), put it between those.

Now look at the second paragraph. 
There are some mathematical things to notice,
such as the set element relation symbol \( \in \) and putting a 
hat on a variable \( \hat{C} \).

One thing to notice is that for the curly braces in sets,
we don't just enter curly braces in the source file.
We have to write backslach brace, \lstinline!\}! and \lstinline!\{!,
to escape from the regular meaning \LaTeX{} has for backslash.  

Another thing to notice is that just as you can't write math as plain text,
you can't write plain text in mathematics; 
it comes out in italics and oddly spaced.
Instead type something like \lstinline!\text{ and }!

But the key takeaway is to treat
mathematical text specially by entering the entire phrase between dollar signs.
That includes even solitary symbols:
contrast the the `C' that starts 
the first sentence of the second paragraph with the set name~$C$. 
The set name is a math symbol 
so put it between dollar signs.

The next document shows more of \LaTeX's work with mathematics.
\lessondoc{lesson3/lesson3doc2}

In the second paragraph of the source document, 
note how we make the super-superscript, and the sub-sub.
In the expression \lstinline!e_{x^2}!,
the superscript of \( e \) is \( x^2 \) so we
have curly braces to group it that way. 

Here are some more examples.
\lessondoc{lesson3/lesson3doc3}



\item[Your PDF viewer]
Mathematical characters give some PDF viewers trouble, particularly
some viewers inside web browsers.
If you are seeing oddball characters, try looking at 
your PDF in Acrobat Reader or some other quite capable reader.  


\item[Next]
The next video includes more mathematics.
See you then!
\end{topic}






% =================
\chapter{More math, including displayed equations}
I'm Fred McGuillicuddy.
This is the fourth in our introduction to \LaTeX{} series.
These videos are from
the free course \url{http://joshua.smcvt.edu/lessons}, 
and you might want to check that out.
Watching the videos without taking the course works, too.

We will continue with looking at 
\LaTeX's capabilities with mathematical text.
\LaTeX{} is the professional standard in mathematics, as well as in 
mathematical areas such as physics and statistics, too. 

Enjoy!



\begin{topic}
\item[\texttt{amsmath}]
In our sample documents, in the preamble,
we've put the line \lstinline!\usepackage{amsmath}!. 
This makes available a set of commands, a package, 
developed by the American Mathematical Society.
Those folks know all about math, 
so we rely on them.


\item[Blackboard bold]
Get the AMS version of the usual symbols for the real numbers, etc., by 
bringing in the \textit{amssymb} package
and using the \lstinline!\mathbb{..}! command. 
\lessondoc{lesson4/lesson4doc1}

\item[Displayed mathematics]
Some mathematical statements are so long, or important, that 
we break them out on a separate line. 
Note that the displayed material has no dollar signs, which would be redundant.
Also, in displayed equations blank lines give an error.

This document shows two fine points about spacing.
In the integral there is a small space between \lstinline!x^2! and 
\lstinline!dx!, 
produced with backslash-comma \lstinline!\,!. 
And, in the final two displayed equations, 
we put \lstinline!\quad! and \lstinline!\qquad! to make separating spaces
(the \lstinline!\qquad! gives twice the space).

The next document shows matrices inside displayed equations. 
In a matrix you put an ampersand between row entries,
and double backslash at each row end.
\lessondoc{lesson4/lesson4doc2}


\item[Splitting displayed equations]
The AMS commands let us split the contents of a displayed equation, 
for instance to align on an equals sign. 
\lessondoc{lesson4/lesson4doc3}

\item[Documentation]
The \texttt{amsmath} package has other environments and 
many other useful commands.
Get the full documentation with a web search for ``amsthm.'' 



\item[Next]
The next video covers writing theorems.
Enjoy!
\end{topic}








% =================
\chapter{Theorems and proofs}
I'm Fred McGuillicuddy with
This is the fifth in our introduction to \LaTeX{} series.
These videos are from
the free certification course \url{http://joshua.smcvt.edu/lessons}, 
and you might want to check that out.
Watching the videos without taking the course works, too.

We will continue looking at writing mathematical text, an area where
\LaTeX{} shines.

\begin{topic}

\item[Theorems]
Mathematical writing uses formal statements, such as theorems and definitions. 
You can get those with the \texttt{amsthm} package.

This document illustrates.
We declare the types of formal statement environment that we want,
what we will call them in the source and what they will be called in the
output.
\lessondoc{lesson5/lesson5doc1}
Notice that the output doesn't say ``Definition 1;'' 
the definition and the example are 
numbered in the same sequence as the theorem.
That's because we declared them with 
the optional argument \lstinline![thm]!,
saying that definitions are numbered in the same sequence as the 
environment defined by \texttt{thm}.

The \texttt{amsthm} package has lots of options for 
fine tuning this stuff.
You can change whether the theorems are numbered separately from the
definitions, for instance, or whether the body of the statement is in
italics or normal type. 
Get the documentation with a web search for ``amsthm.'' 


\item[Cheat sheet]
That ends our introduction to writing mathematics with \LaTeX.
Before we leave, though, notice that 
included on the lesson page is a cheat sheet for the mathematics that
an undergraduate should need, including symbols and some 
constructs such as matrices and displayed equations.
Hope it helps!


\item[Next]
Next is lists and tables.
See you then.
\end{topic}





% =================
\chapter{Lists and Tables}
I'm Fred McGuillicuddy with the
sixth tutorial in our eleven-video introduction to \LaTeX{} series.
These videos are from
the free certification course \url{http://joshua.smcvt.edu/lessons}.
Watching these without taking the course works also.
Enjoy!

\begin{topic}
\item[Lists]
To make a numbered list use the \lstinline!enumerate! environment.
Unnumbered lists, bullet point lists, use \lstinline!itemize!.
\lessondoc{lesson6/lesson6doc1}
There is one more list environment, for descriptions or definitions.
It uses the optional argument to \lstinline!\item!.
\lessondoc{lesson6/lesson6doc2}


\item[Tables]
Make a table with the \lstinline!tabular! environment.
It starts with you specifing 
how the are columns aligned: left, centered, or right, 
for instance,
and whether you want vertical bars between columns.
\lessondoc{lesson6/lesson6doc3}
In the body of the table, separate items with ampersands \&.
Separate rows with double backslashes.
Generate a horizontal line with \lstinline!\hline!.

There are two fine points about tables that come in handy.
First, you sometimes want a horizontal line that spans only some of the columns.
Use a \lstinline!\cline{..}!.

This document shows two kinds of column adjustment. 
\lessondoc{lesson6/lesson6doc4}
The specifier \lstinline!r@{$.$}l! 
shows numbers aligned on the decimal point 
(the at-sign \lstinline!@{..}! puts material between columns).
With this, \lstinline!&$1$ &$40$! produces the output: $1.40$, with 
the costs centered on the decimal point in that column.  

Now lets look at \lstinline!\multicolumn{..}{..}{..}!, which allows 
an entry to span more than one column.
The first argument is the number of columns spanned
and the third is the material to put in that span.
The second argument says how to typeset the material. 
Thus the  \lstinline!multicolumn{2}{c}{\textit{cost}}!,
causes the word ``cost'' to span the two columns and be centered and in italics.
The other one, \lstinline!multicolumn{1}{c}{}!, is tricky but useful.
It eliminates the vertical line between the first two columns in that row, 
because it does not say \lstinline!c|!, it says \lstinline!c!.

Sometimes a table's entry contains a lot of  text.
To get \LaTeX{} to break lines, as in a paragraph, use   
a specifier like \lstinline!p{1.5in}!. 
\lessondoc{lesson6/lesson6doc5}
We've allowed a ragged right margin, and so
we brought in the package \lstinline!ragged2e! and used the
declaration \lstinline!RaggedRight!.
We group the area with curly braces, 
so \LaTeX{} knows what text to set ragged.

There are many packages to do more advanced things with tables,
such as cover more than one page, or shade alternate lines.
Those lie beyond our scope here.




\item[Next]
Next lesson is on color and graphics.
See you!
\end{topic}






% =================
\chapter{Color, external graphics, and figures}
I'm Fred McGuillicuddy.
Welcome to the seventh video tutorial in our introduction to \LaTeX{}.
These are from
the free certification course \url{http://joshua.smcvt.edu/lessons}.
Watching without taking the course works, too.


\begin{topic}
\item[Color] 
To get colors use the \lstinline!xcolor! package
\lessondoc{lesson7/lesson7doc1}

The standard colors, besides white and black, are 
red, green, and blue, as well as cyan, magenta, and yellow.
You can get any color at all, just do a web search for
``latex xcolor.''

\item[The graphicx package]
Often you want to include a graphic, such as a photograph or
a data graph from an external program.
That graphic might be in a PNG, JPG, or PDF file.
Here we include some plots that were generated with a tool 
called Asymptote that are
contained in the files \lstinline!plotsin.pdf! and \lstinline!plotcos.pdf!.
\lessondoc{lesson7/lesson7doc2}
Note that we can adjust the height or width of the graphic. 
(Graphics come in two kinds: vector and raster.
The graphics here are vector, meaning that we can enlarge them or
shrink as much as we like.
If they had been JPG files then changing the size would probably 
produce artifacts such as jaggies.
Such things are not a fault of \LaTeX{} but instead have to do with the input
file.
If you have an option, for instance with technical drawings,
choose vector-based graphics.)

The graphic has to be
in a directory where \LaTeX{} can find it.
The simplest place is the same directory as the file, but there are other
options.
For more information do a web search.



\item[Figures]
What if you want to put something, such as a graphic or a table, 
and it ends so near the top 
or bottom of a page that there isn't room?
You don't want it to stick off the page. 
The tradition in 
technical documents is to move the illustrations to a later page.
\LaTeX{} handles this with the \lstinline!figure! environment. 

The issues involved are complex~--- for instance, in deciding what fits, 
\LaTeX{} has to worry about whether things have migrateed in from 
prior pages, but this illustrates the principle.
\lessondoc{lesson7/lesson7doc3}
Note the caption.
Note also the \lstinline!centering! to put the graphic in the 
middle of the figure.

The \lstinline!figure!
environment takes some optional arguments that 
influence where the floats go.
Some are: 
`h' which tries to put the float about where it occurs 
in the source text, 
`t' that tries to put it at the top of a page, 
`b' that tries to put it at the bottom, and 
`p' that puts it on a special page for floats only.
However, the algorithm is complex and the results are 
sometimes surprising.

We'll mention that the \texttt{float} package provides an option 
`\lstinline!H!' that will keep figures from floating.
But if you use it you will see why they are designed to float, since you
quickly get figures that are in awkward places.



\item[Next]
This lesson had you bring in graphics from outside the document.
Next is making your own, inside the document.
\end{topic}






% =================
\chapter{Graphics inside the document}
I'm Fred McGuillicuddy.
Welcome to the eigth tutorial in our introduction to \LaTeX{}.
These videos are from
the certification course \url{http://joshua.smcvt.edu/lessons}.
Watching the videos without taking the course is fine, too.
Enjoy!

\begin{topic}
\item[Introduction] 
The prior video showed how to bring graphics into a \LaTeX{} document.
You can also draw graphics inside the document using a package called 
\tikzname.
This has many advantages; for instance, the fonts in the graphic are 
sure to match the ones in the document.

\tikzname{} is too full-featured to cover here.
Instead we will see a number of examples to give you a taste.
See the excellent documentation, which you 
can find with a web search for ``\texttt{tikz documentation}''.

First is a graph of the cosine function.
The action takes place inside the \lstinline!tikzpicture! environment.
\tikzname{} calculates the value of the function at a number
of points and interpolates to plot the graph.
\lessondoc{lesson8/lesson8doc1}  
(The variable \lstinline!\x! is in radians because of the \lstinline!r!
at the end of that line.)

The next example is a Venn diagram, showing area filling.
\lessondoc{lesson8/lesson8doc2}

Next is simple graph, an automata, with labelled edges. 
See how you tell \tikzname{} the coordinates of the nodes 
\texttt{(A)}, \texttt{(B)}, etc., 
and then you draw edges by specifying the node names 
rather than repeat the coordinates. 
\lessondoc{lesson8/lesson8doc3}

The final example is showing off, giving you a sense of the capabilities 
of \tikzname{} that make it a professional tool.
\lessondoc{lesson8/lesson8doc4}
Isn't that nice?
Note the use of color to make the parts of the graphic clear, and
note also the plot command that smoothly connects the coordinates.



\item[Next]
Our next lesson is on cross-references and the table of contents.
This is the eighth lesson of eleven, so we are getting close to the end!
\end{topic}






% =================
\chapter{More structure: cross-references, 
       table of contents}
I'm Fred McGuillicuddy.
Welcome to the ninth tutorial in our introduction to \LaTeX{} series.
These are from
our certification course \url{http://joshua.smcvt.edu/lessons}.
Watching the videos without taking the course is fine, too.
Enjoy!



\begin{topic}
\item[Cross referencing]
A cross-reference is where you tell the reader to ``See Figure 2.7'' or 
``See page 9.''
It involves two steps: you put a 
\lstinline!\label{..}! at the spot to which you will point the reader, 
and you put a \lstinline!\ref{..}! or \lstinline!\pageref{..}! where you
want the reference to appear.
\lessondoc{lesson9/lesson9doc1}
The tie character~\lstinline!~! ensures that no line break happens
between the word ``Figure'' or ``page'' or ``Equation'' and the reference.

\LaTeX{} keeps cross-reference in an auxiliary file and sometimes
it takes a second run to have that file up to date.
You'll know this is happening when instead of a cross-reference 
you see two boldface question marks.
Just compile again.



\item[Table of contents]  
To get a Table of Contents you tell \LaTeX{} where to put it.
\lessondoc{lesson9/lesson9doc2}
Compile the document twice to get the table of contents to appear, 
just as with the cross references.

\item[Your own commands]
You can help structure your document by making some simple commands.
For instance, you may often write the name of your institution and so 
you may save some typing, and misspelling, by making a new command
\lstinline!\newcommand{\mc}{My College}!.

Your commands can take arguments. 
An example is that some authors like the `d' in $dx$ to be upright,
and may make a command like this
\lstinline!\newcommand{\diff}[1]{\text{d}#1}!,
invoked as \lstinline!$\int x^2\diff{x}$!.

You can also create your own environment with a command like this.
\begin{lstlisting}
  \newenvironment{exerciseanswer}{\textbf{Answer} }{\textbf{Answer end}}
\end{lstlisting}
This puts ``Answer'' at the start, with a space, and ``Answer end''
at the end.

But before you create many of these, note that often \LaTeX{} already has
such commands for most everyday purposes.
You will also want to look for relevant packages, 
which we will discuss later, in the eleventh video.

That \lstinline!\newcommand{..}! says `new' because it first checks
whether \LaTeX{} already has a command by that name.
You can change an existing command with \lstinline!\renewcommand{..}!.
Commands such as these can do very many, very powerful things,
but saying more would take us too far afield.



\item[Next]
Next we will cover two topics: listing computer code, and a bibliography.
See you then!
\end{topic}








% =================
\chapter{Code listings, and bibliography}
I'm Fred McGuillicuddy.
This is the tenth tutorial in our introduction to \LaTeX{} series.
These videos are from
our free certification course \url{https://www.tug.org/lessons}.
Watching without taking the course works great also.
Enjoy!

\begin{topic}
\item[\texttt{listings}]
To include source code listings, use the \lstinline!listings! package.
You can then put listings inside a \lstinline!lstlisting! environment
where they will be reproduced verbatim. 
\lessondoc{lesson10/lesson10doc1}

Notice the \lstinline!\lstset! command.
It configures the output.
The \lstinline!basicstyle! option puts the code in
typewriter type, and at a smaller size.
This command has many options; 
see the documentation.

If you copy and paste the computer code into your document then 
you run the risk that the code will later change and leave your document out
of sync. 
You may want to instead take your code directly from its file, with
the \lstinline!\lstinputlistings{..}!.

\item[Bibliography]
Professional papers often need to have a bibliography.
\LaTeX{} offers a facility that is powerful and flexible.
But for writing undergraduate papers, simple is better.
\lessondoc{lesson10/lesson10doc2}
As with other cross references, 
if you see boldface double question marks where you expect a citation
then compile the document again.

The reference \lstinline!bibitem! has the book's information. 
Note the citation \lstinline!\cite{feynman}! in the text
matches the \lstinline!\bibitem{feynman}!.

The command \lstinline!\begin{thebibliography}{9}! has that somewhat odd 9.
It tells \LaTeX{} how much space to set aside for the numbers of the cited
works.
In our list of references there is one item, so we have one 9 in there.
If the number of referenceds was between ten and ninety-nine then we would write
\lstinline!\begin{thebibliography}{99}!.
Possible three-digit references would require three 9's. 

\textit{Comment about a more sophisticated approach.}
The above suffices for most undergraduate work.
But the \TeX{} software family has a more sophisticated tool for 
advanced authors.
For instance, some authors 
have a database of many source references and for any particular 
paper they want to pull only the references actually used.
Or, an author may want to select the 
style of reference for a desired journal, 
without having to by-hand reformat.
For these, the \TeX{} family has has a program called \texttt{bibtex}.
For more informaion, do a web search.



\item[Next]
Our next lesson is our final video.
We wrap up by discussing how packages extend \LaTeX's capabilities,
and introduce a package to make presentaion slides.
\end{topic}






% =================
\chapter{Packages and the community}
I'm Fred McGuillicuddy from the \TeX{} Users Group.
Welcome to the eleventh and final tutorial 
in our introduction to \LaTeX{} series.
These are from
our certification course \url{http://joshua.smcvt.edu/lessons}.
Watching the videos without taking the course works great, too.
This is the final video in the series.
Hope the series helped you.
Enjoy!

\begin{topic}
\item[Packages]
One of \LaTeX{}'s strengths is the community of people behind it.
First, there is the \TeX{} Users Group, who provide these lessons. 
Another example is many add-ons, called ``packages,'' that are 
available to solve your problems.

% For instance, there is a package \texttt{cleverref} that remembers 
% what a reference is, so that instead of typing 
% \lstinline!example~\ref{ex:curves}! you need only type 
% \lstinline!cref{ex:curves}!.
% And there is a package \lstinline!enumitem! to modify the appearances of lists,
% for instance replacing `1.' with `a)'.

An example is the package for adding hyperreferences, that is, web links, 
to your documents. 
\lessondoc{lesson11/lesson11doc1}
Compile this document twice.
Note that the table of contents is made of links to the sections.
Note also that we can make links outside the document as well,
that bring up your system's web browser.


\item[CTAN]
There is a central source for all this package goodness.
At \url{http://www.ctan.org} you will see thousands
of packages, and much more, that is free for you to use.
Think of a topic such as ``chemistry'' or ``exams,''
and type it into the search box.
Odds are very good that you will find something that helps you.

Visiting CTAN gives you a sense of the sheer number and variety 
of solutions available.
If you find a package that suits your needs, before you download and install 
note that most likley you already have access to that package,
no matter whether you downloaded a system for your computer 
or you use an online site;
both will have almost total complete collections of the package on CTAN
(except those whose license is an issue).


\item[Beamer]  
One more example of a solution that is freely available:
you can make slides for a presentation and have access to all 
of \LaTeX{}'s capabilities.
\lessondoc{lesson11/lesson11doc2}
The first line is \lstinline!\documentclass{beamer}!,
so this is a class, like ``article.''
Note also that the \lstinline!\pause! commands cause the items to be 
displayed one at a time, so that you click to uncover the next one.

This package is extremely sophisticated, with too many options for us
to cover here.
The point of showing it to you, besides just so you know it is there,  
is to illustrate that there are a great many amazing things available. 

\item[Closing]
We hope you have found the lessons useful.
For more information, please visit the \TeX{} Users Group home page.
If you find it useful, consider joining TUG.
Happy \LaTeX-ing!
\end{topic}




% =================
\chapter{Certification}

This series is a quick tutorial.
For a longer introduction see 
\href{https://mirror.ctan.org/tex-archive/info/lshort/english/lshort_letter.pdf}{\textit{The Not So Short Introduction to \LaTeX 2e}}.
Two other very useful documents are
\href{http://mirrors.ctan.org/macros/latex/required/amsmath/amsmath.pdf}{\textit{Users Guide for the \texttt{amsmath} Package (Version 2.0)}}
and 
\href{http://mirrors.ctan.org/info/symbols/comprehensive/symbols-letter.pdf}{\textit{The Comprehensive \LaTeX{} Symbol List}}.
All three documents are free.

Take the test.
\end{document}
